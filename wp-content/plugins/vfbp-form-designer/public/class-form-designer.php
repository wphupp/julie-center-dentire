<?php
/**
 * The main function for the add-on
 *
 * @since      2.0
 */
class VFB_Pro_Addon_Form_Designer_Main {
	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'output' ) );
	}

	/**
	 * output function.
	 *
	 * @access public
	 * @return void
	 */
	public function output() {
		$css = '';

		$vfbdb    = new VFB_Pro_Data();
		$forms    = $vfbdb->get_all_forms();

		if ( is_array( $forms ) && !empty( $forms ) ) {
			foreach ( $forms as $form ) {
				$form_id  = $form['id'];
				$settings = $vfbdb->get_addon_settings( $form_id );

				$design_enable = isset( $settings['form-design-enable'] ) ? $settings['form-design-enable'] : '';

				if ( empty( $design_enable ) )
					continue;

				// Font Options
				$font_text     = isset( $settings['form-design']['design-font-text']     ) ? $settings['form-design']['design-font-text']     : 'Helvetica';
				$font_size     = isset( $settings['form-design']['design-font-size']     ) ? $settings['form-design']['design-font-size']     : '14';
				$font_family   = $this->font_family( $font_text );

				// Label Styles
				$label_style   = isset( $settings['form-design']['design-label-style']   ) ? $settings['form-design']['design-label-style']   : 'bold';
				$label_spacing = isset( $settings['form-design']['design-label-spacing'] ) ? $settings['form-design']['design-label-spacing'] : '5';
				$label_color   = isset( $settings['form-design']['design-label-color']   ) ? $settings['form-design']['design-label-color']   : '#444';

				// Input Styles
				$input_text_color    = isset( $settings['form-design']['design-input-text-color']    ) ? $settings['form-design']['design-input-text-color']    : '#555';
				$input_bg_color      = isset( $settings['form-design']['design-input-bg-color']      ) ? $settings['form-design']['design-input-bg-color']      : '#fff';
				$placeholder_color   = isset( $settings['form-design']['design-placeholder-color']   ) ? $settings['form-design']['design-placeholder-color']   : '#777';
				$input_border_width  = isset( $settings['form-design']['design-input-border-width']  ) ? $settings['form-design']['design-input-border-width']  : '1';
				$input_border_style  = isset( $settings['form-design']['design-input-border-style']  ) ? $settings['form-design']['design-input-border-style']  : 'solid';
				$input_border_color  = isset( $settings['form-design']['design-input-border-color']  ) ? $settings['form-design']['design-input-border-color']  : '#ccc';
				$input_border_radius = isset( $settings['form-design']['design-input-border-radius'] ) ? $settings['form-design']['design-input-border-radius'] : '4';
				$input_focus_color   = isset( $settings['form-design']['design-input-focus-color']   ) ? $settings['form-design']['design-input-focus-color']   : '#66afe9';
				$input_focus_shadow  = isset( $settings['form-design']['design-input-focus-shadow']  ) ? $settings['form-design']['design-input-focus-shadow']  : true;

				// Descriptions
				$desc_text_color         = isset( $settings['form-design']['design-desc-text-color']         ) ? $settings['form-design']['design-desc-text-color']         : '#777';
				$desc_vertical_margin    = isset( $settings['form-design']['design-desc-vertical-margin']    ) ? $settings['form-design']['design-desc-vertical-margin']    : '5';
				$desc_horizontal_margin  = isset( $settings['form-design']['design-desc-horizontal-margin']  ) ? $settings['form-design']['design-desc-horizontal-margin']  : '0';
				$desc_vertical_padding   = isset( $settings['form-design']['design-desc-vertical-padding']   ) ? $settings['form-design']['design-desc-vertical-padding']   : '0';
				$desc_horizontal_padding = isset( $settings['form-design']['design-desc-horizontal-padding'] ) ? $settings['form-design']['design-desc-horizontal-padding'] : '0';

				// Validation Styles
				$invalid_text_color   = isset( $settings['form-design']['design-invalid-text-color']   ) ? $settings['form-design']['design-invalid-text-color']   : '#a94442';
				$invalid_border_color = isset( $settings['form-design']['design-invalid-border-color'] ) ? $settings['form-design']['design-invalid-border-color'] : '#a94442';
				$valid_text_color     = isset( $settings['form-design']['design-valid-text-color']     ) ? $settings['form-design']['design-valid-text-color']     : '#3c763d';
				$valid_border_color   = isset( $settings['form-design']['design-valid-border-color']   ) ? $settings['form-design']['design-valid-border-color']   : '#3c763d';

				// Buttons
				$btn_text_color         = isset( $settings['form-design']['design-btn-text-color']         ) ? $settings['form-design']['design-btn-text-color']         : '#fff';
				$btn_bg_color           = isset( $settings['form-design']['design-btn-bg-color']           ) ? $settings['form-design']['design-btn-bg-color']           : '#337ab7';
				$btn_font_size          = isset( $settings['form-design']['design-btn-font-size']          ) ? $settings['form-design']['design-btn-font-size']          : '14';
				$btn_font_weight        = isset( $settings['form-design']['design-btn-font-weight']        ) ? $settings['form-design']['design-btn-font-weight']        : 'normal';
				$btn_border_width       = isset( $settings['form-design']['design-btn-border-width']       ) ? $settings['form-design']['design-btn-border-width']       : '1';
				$btn_border_style       = isset( $settings['form-design']['design-btn-border-style']       ) ? $settings['form-design']['design-btn-border-style']       : 'solid';
				$btn_border_color       = isset( $settings['form-design']['design-btn-border-color']       ) ? $settings['form-design']['design-btn-border-color']       : '#2e6da4';
				$btn_border_radius      = isset( $settings['form-design']['design-btn-border-radius']      ) ? $settings['form-design']['design-btn-border-radius']      : '4';
				$btn_hover_text_color   = isset( $settings['form-design']['design-btn-hover-text-color']   ) ? $settings['form-design']['design-btn-hover-text-color']   : '#fff';
				$btn_hover_bg_color     = isset( $settings['form-design']['design-btn-hover-bg-color']     ) ? $settings['form-design']['design-btn-hover-bg-color']     : '#286090';
				$btn_hover_border_color = isset( $settings['form-design']['design-btn-hover-border-color'] ) ? $settings['form-design']['design-btn-hover-border-color'] : '#204d74';

				// CSS
				$custom_css = isset( $settings['form-design']['design-css'] ) ? $settings['form-design']['design-css'] : '';

				$form_prefix = "#vfbp-form-$form_id";

				// Font Options
				$css .= sprintf(
					'%1$s {' .
					'font-family: %2$s;' .
					'font-size: %3$spx;' .
					'}',
					$form_prefix,
					$font_family,
					$font_size
				);

				// Label Styles
				$css .= sprintf(
					'%1$s.vfbp-form label {' .
					'font-weight: %2$s;' .
					'margin-bottom: %3$spx;' .
					'color: %4$s;' .
					'}',
					$form_prefix,
					$label_style,
					$label_spacing,
					$label_color
				);

				// Reset certain Radio and Address label styles back to normal
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-radio label,' .
					'%1$s.vfbp-form label.vfb-address-label {' .
					'font-weight: normal;' .
					'}' .
					'%1$s.vfbp-form .vfb-radio label {' .
					'margin:0;' .
					'}',
					$form_prefix
				);

				// Input Styles
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-form-control {' .
					'color: %2$s;' .
					'background-color: %3$s;' .
					'border: %4$spx %5$s %6$s;' .
					'border-radius: %7$spx;' .
					'}',
					$form_prefix,
					$input_text_color,
					$input_bg_color,
					$input_border_width,
					$input_border_style,
					$input_border_color,
					$input_border_radius
				);

				// Box shadow
				$box_shadow = ( false == $input_focus_shadow ) ? 'box-shadow: none;' : '';

				// Input focus color
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-form-control:focus {' .
					'border-color: %2$s;' .
					$box_shadow .
					'}',
					$form_prefix,
					$input_focus_color
				);

				// Placeholders
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-form-control::-moz-placeholder {color: %2$s;}' .
					'%1$s.vfbp-form .vfb-form-control::-webkit-input-placeholder {color: %2$s;}' .
					'%1$s.vfbp-form .vfb-form-control:-ms-input-placeholder {color: %2$s;}',
					$form_prefix,
					$placeholder_color
				);

				// Descriptions
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-help-block {' .
					'color: %2$s;' .
					'margin: %3$spx %4$spx;' .
					'padding: %5$spx %6$spx;' .
					'}',
					$form_prefix,
					$desc_text_color,
					$desc_vertical_margin,
					$desc_horizontal_margin,
					$desc_vertical_padding,
					$desc_horizontal_padding
				);

				// Validation Styles
				$css .= sprintf(
					'%1$s.vfbp-form .vfb-has-error .vfb-help-block,' .
					'%1$s.vfbp-form .vfb-has-error .vfb-control-label,' .
					'%1$s.vfbp-form .vfb-help-block .parsley-required {' .
					'color: %2$s;' .
					'}' .
					'%1$s.vfbp-form .vfb-has-error .vfb-form-control {' .
					'border-color: %3$s;' .
					'}' .
					'%1$s.vfbp-form .vfb-has-success .vfb-help-block,' .
					'%1$s.vfbp-form .vfb-has-success .vfb-control-label {' .
					'color: %4$s;' .
					'}' .
					'%1$s.vfbp-form .vfb-has-success .vfb-form-control {' .
					'border-color: %5$s;' .
					'}',
					$form_prefix,
					$invalid_text_color,
					$invalid_border_color,
					$valid_text_color,
					$valid_border_color
				);

				// Buttons
				$css .= sprintf(
					'%1$s.vfbp-form .btn-primary {' .
					'color: %2$s;' .
					'background-color: %3$s;' .
					'font-size: %4$spx;' .
					'font-weight: %5$s;' .
					'border: %6$spx %7$s %8$s;' .
					'border-radius: %9$spx;' .
					'}',
					$form_prefix,
					$btn_text_color,
					$btn_bg_color,
					$btn_font_size,
					$btn_font_weight,
					$btn_border_width,
					$btn_border_style,
					$btn_border_color,
					$btn_border_radius
				);

				// Buttons hover
				$css .= sprintf(
					'%1$s.vfbp-form .btn-primary:hover {' .
					'color: %2$s;' .
					'background-color: %3$s;' .
					'border-color: %4$s;' .
					'}',
					$form_prefix,
					$btn_hover_text_color,
					$btn_hover_bg_color,
					$btn_hover_border_color
				);

				// Custom CSS
				$css .= $custom_css;
			}

			wp_add_inline_style( 'vfb-pro', $css );
		}
	}

	/**
	 * font_family function.
	 *
	 * @access private
	 * @param mixed $font
	 * @return void
	 */
	private function font_family( $font ) {
		if ( empty( $font ) )
			return $font;

		$stack = '';
		switch ( $font ) {
			// Sans Serif
			case 'Arial' :
				$stack = "Arial, 'Helvetica Neue', Helvetica, sans-serif";
				break;

			case 'Arial Black' :
				$stack = "'Arial Black', 'Arial Bold', Gadget, sans-serif";
				break;

			case 'Arial Narrow' :
				$stack = "'Arial Narrow', Arial, sans-serif";
				break;

			case 'Arial Rounded MT Bold' :
				$stack = "'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif";
				break;

			case 'Avant Garde' :
				$stack = "'Avant Garde', Avantgarde, 'Century Gothic', CenturyGothic, AppleGothic, sans-serif";
				break;

			case 'Calibri' :
				$stack = "Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif";
				break;

			case 'Candara' :
				$stack = "Candara, Calibri, Segoe, 'Segoe UI', Optima, Arial, sans-serif";
				break;

			case 'Century Gothic' :
				$stack = "'Century Gothic', CenturyGothic, AppleGothic, sans-serif";
				break;

			case 'Franklin Gothic Medium' :
				$stack = "'Franklin Gothic Medium', 'Franklin Gothic', 'ITC Franklin Gothic', Arial, sans-serif";
				break;

			case 'Futura' :
				$stack = "Futura, 'Trebuchet MS', Arial, sans-serif";
				break;

			case 'Geneva' :
				$stack = "Geneva, Tahoma, Verdana, sans-serif";
				break;

			case 'Gill Sans' :
				$stack = "'Gill Sans', 'Gill Sans MT', Calibri, sans-serif";
				break;

			case 'Helvetica' :
				$stack = "'Helvetica Neue', Helvetica, Arial, sans-serif";
				break;

			case 'Impact' :
				$stack = "Impact, Haettenschweiler, 'Franklin Gothic Bold', Charcoal, 'Helvetica Inserat', 'Bitstream Vera Sans Bold', 'Arial Black', 'sans serif'";
				break;

			case 'Lucida Grande' :
				$stack = "'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Geneva, Verdana, sans-serif";
				break;

			case 'Optima' :
				$stack = "Optima, Segoe, 'Segoe UI', Candara, Calibri, Arial, sans-serif";
				break;

			case 'Segoe UI' :
				$stack = "'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif";
				break;

			case 'Tahoma' :
				$stack = "Tahoma, Verdana, Segoe, sans-serif";
				break;

			case 'Trebuchet MS' :
				$stack = "'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif";
				break;

			case 'Verdana' :
				$stack = "Verdana, Geneva, sans-serif";
				break;

			// Serif
			case 'Big Caslon' :
				$stack = "'Big Caslon', 'Book Antiqua', 'Palatino Linotype', Georgia, serif";
				break;

			case 'Bodoni MT' :
				$stack = "'Bodoni MT', Didot, 'Didot LT STD', 'Hoefler Text', Garamond, 'Times New Roman', serif";
				break;

			case 'Book Antiqua' :
				$stack = "'Book Antiqua', Palatino, 'Palatino Linotype', 'Palatino LT STD', Georgia, serif";
				break;

			case 'Calisto MT' :
				$stack = "'Calisto MT', 'Bookman Old Style', Bookman, 'Goudy Old Style', Garamond, 'Hoefler Text', 'Bitstream Charter', Georgia, serif";
				break;

			case 'Cambria' :
				$stack = "Cambria, Georgia, serif";
				break;

			case 'Didot' :
				$stack = "Didot, 'Didot LT STD', 'Hoefler Text', Garamond, 'Times New Roman', serif";
				break;

			case 'Garamond' :
				$stack = "Garamond, Baskerville, 'Baskerville Old Face', 'Hoefler Text', 'Times New Roman', serif";
				break;

			case 'Georgia' :
				$stack = "Georgia, Times, 'Times New Roman', serif";
				break;

			case 'Goudy Old Style' :
				$stack = "'Goudy Old Style', Garamond, 'Big Caslon', 'Times New Roman', serif";
				break;

			case 'Hoefler Text' :
				$stack = "'Hoefler Text', 'Baskerville Old Face', Garamond, 'Times New Roman', serif";
				break;

			case 'Lucida Bright' :
				$stack = "'Lucida Bright', Georgia, serif";
				break;

			case 'Palatino' :
				$stack = "Palatino, 'Palatino Linotype', 'Palatino LT STD', 'Book Antiqua', Georgia, serif";
				break;

			case 'Perpetua' :
				$stack = "Perpetua, Baskerville, 'Big Caslon', 'Palatino Linotype', Palatino, 'URW Palladio L', 'Nimbus Roman No9 L', serif";
				break;

			case 'Rockwell' :
				$stack = "Rockwell, 'Courier Bold', Courier, Georgia, Times, 'Times New Roman', serif";
				break;

			case 'Rockwell Extra Bold' :
				$stack = "'Rockwell Extra Bold', 'Rockwell Bold', monospace";
				break;

			case 'Baskerville' :
				$stack = "Baskerville, 'Baskerville Old Face', 'Hoefler Text', Garamond, 'Times New Roman', serif";
				break;

			case 'Times New Roman' :
				$stack = "TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif";
				break;

			// Monospaced
			case 'Consolas' :
				$stack = "Consolas, monaco, monospace";
				break;

			case 'Courier New' :
				$stack = "'Courier New', Courier, 'Lucida Sans Typewriter', 'Lucida Typewriter', monospace";
				break;

			case 'Lucida Console' :
				$stack = "'Lucida Console', 'Lucida Sans Typewriter', monaco, 'Bitstream Vera Sans Mono', monospace";
				break;

			case 'Lucida Sans Typewriter' :
				$stack = "'Lucida Sans Typewriter', 'Lucida Console', monaco, 'Bitstream Vera Sans Mono', monospace";
				break;

			case 'Monaco' :
				$stack = "monaco, Consolas, 'Lucida Console', monospace";
				break;

			// Fantasy
			case 'Copperplate' :
				$stack = "Copperplate, 'Copperplate Gothic Light', fantasy";
				break;

			case 'Papyrus' :
				$stack = "Papyrus, fantasy";
				break;

			// Script
			case 'Brush Script MT' :
				$stack = "'Brush Script MT', cursive";
				break;
		}

		return $stack;
	}
}