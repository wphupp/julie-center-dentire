<?php
/**
 * Class that handles all AJAX calls and admin settings
 *
 * This is called directly from vfb-pro/admin/class-addons.php and
 * vfb-pro/admin/class-ajax.php
 *
 * @since 3.0
 */
class VFB_Pro_Addon_Create_User_Admin_Settings {

	/**
	 * mobile_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function settings( $data, $form_id ) {
		$user_enable = isset( $data['user-enable']       ) ? $data['user-enable']       : '';
		$user_notify = isset( $data['user-notify-email'] ) ? $data['user-notify-email'] : '';
		$user_role   = isset( $data['user-role']         ) ? $data['user-role']         : '';
		$user_email  = isset( $data['user-email']        ) ? $data['user-email']        : '';
		$username    = isset( $data['user-username']     ) ? $data['user-username']     : '';
		$password    = isset( $data['user-password']     ) ? $data['user-password']     : '';

		$vfbdb           = new VFB_Pro_Data();
		$email_fields    = $vfbdb->get_fields( $form_id, "AND field_type = 'email' ORDER BY field_order ASC" );
		$username_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'username' ORDER BY field_order ASC" );
		$password_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'password' ORDER BY field_order ASC" );
	?>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row">
					<label for="user-enable"><?php _e( 'Enable Create User' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<fieldset>
						<label>
							<input type="hidden" name="settings[user-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
							<input type="checkbox" name="settings[user-enable]" id="user-enable" value="1"<?php checked( $user_enable, 1 ); ?> /> <?php _e( "Enable Create User for this form.", 'vfbp-create-user' ); ?>
						</label>
					</fieldset>
				</td>
			</tr>
		</tbody>
		<tbody class="vfb-create-user-settings<?php echo !empty( $user_enable ) ? ' active' : ''; ?>">
			<tr valign="top">
				<th scope="row">
					<label for="user-notify-email"><?php _e( 'Send Email Notification' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<fieldset>
						<label>
							<input type="hidden" name="settings[user-notify-email]" value="0" /> <!-- This sends an unchecked value to the meta table -->
							<input type="checkbox" name="settings[user-notify-email]" id="user-notify-email" value="1"<?php checked( $user_notify, 1 ); ?> /> <?php _e( "Send the WordPress New User notification email.", 'vfbp-create-user' ); ?>
						</label>
					</fieldset>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="user-role"><?php _e( 'User Role' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<select name="settings[user-role]" id="user-role">
						<?php wp_dropdown_roles( $user_role ); ?>
					</select>

					<p class="description"><?php _e( 'Set the default user role for users created with this form.' , 'vfbp-create-user' ); ?></p>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="user-email"><?php _e( 'Email Field' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<select id="user-email" name="settings[user-email]">
						<option value=""<?php selected( '', $user_email ); ?>></option>
						<?php
							if ( is_array( $email_fields ) && !empty( $email_fields ) ) {
								foreach ( $email_fields as $email ) {
									$label    = isset( $email['data']['label'] ) ? $email['data']['label'] : '';
									$field_id = $email['id'];

									printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $user_email, false ) );
								}
							}
							else {
								printf( '<option>(%s)</option>', __( 'No Email Fields Found', 'vfbp-create-user' ) );
							}
						?>
					</select>
					<p class="description"><?php _e( 'An email address is required to create a new user.' , 'vfbp-create-user' ); ?></p>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="user-username"><?php _e( 'Username Field' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<select id="user-username" name="settings[user-username]">
						<option value=""<?php selected( '', $username ); ?>></option>
						<?php
							if ( is_array( $username_fields ) && !empty( $username_fields ) ) {
								foreach ( $username_fields as $user ) {
									$label    = isset( $user['data']['label'] ) ? $user['data']['label'] : '';
									$field_id = $user['id'];

									printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $username, false ) );
								}
							}
							else {
								printf( '<option>(%s)</option>', __( 'No Username Fields Found', 'vfbp-create-user' ) );
							}
						?>
					</select>
					<p class="description"><?php _e( 'A username is required to create a new user.' , 'vfbp-create-user' ); ?></p>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="user-password"><?php _e( 'Password Field' , 'vfbp-create-user' ); ?></label>
				</th>
				<td>
					<select id="user-password" name="settings[user-password]">
						<option value=""<?php selected( '', $password ); ?>></option>
						<?php
							if ( is_array( $password_fields ) && !empty( $password_fields ) ) {
								foreach ( $password_fields as $pass ) {
									$label    = isset( $pass['data']['label'] ) ? $pass['data']['label'] : '';
									$field_id = $pass['id'];

									printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $password, false ) );
								}
							}
							else {
								printf( '<option>(%s)</option>', __( 'No Password Fields Found', 'vfbp-create-user' ) );
							}
						?>
					</select>
					<p class="description"><?php _e( 'A password is required to create a new user.' , 'vfbp-create-user' ); ?></p>
				</td>
			</tr>
		</tbody>
	</table>
	<?php
	}
}