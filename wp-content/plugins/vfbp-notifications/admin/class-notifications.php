<?php
/**
 * The main function for the add-on
 *
 * @since      2.0
 */
class VFB_Pro_Addon_Notifications_Main {
	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'vfbp_after_save_entry', array( $this, 'init' ), 10, 2 );
	}

	/**
	 * init function.
	 *
	 * @access public
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function init( $entry_id, $form_id ) {
		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$cell_enable             = isset( $settings['cell-enable']             ) ? $settings['cell-enable']             : '';
		$mailchimp_enable        = isset( $settings['mailchimp-enable']        ) ? $settings['mailchimp-enable']        : '';
		$campaign_monitor_enable = isset( $settings['campaign-monitor-enable'] ) ? $settings['campaign-monitor-enable'] : '';
		$highrise_enable         = isset( $settings['highrise-enable']         ) ? $settings['highrise-enable']         : '';
		$freshbooks_enable       = isset( $settings['freshbooks-enable']       ) ? $settings['freshbooks-enable']       : '';

		// Cell phone
		if ( !empty( $cell_enable ) )
			$this->mobile_phone( $settings, $entry_id, $form_id );

		// MailChimp
		if ( !empty( $mailchimp_enable ) )
			$this->mailchimp( $settings, $entry_id, $form_id );

		// Campaign Monitor
		if ( !empty( $campaign_monitor_enable ) )
			$this->campaign_monitor( $settings, $entry_id, $form_id );

		// Highrise
		if ( !empty( $highrise_enable ) )
			$this->highrise( $settings, $entry_id, $form_id );

		// Freshbooks
		if ( !empty( $freshbooks_enable ) )
			$this->freshbooks( $settings, $entry_id, $form_id );
	}

	/**
	 * mobile_phone function.
	 *
	 * @access private
	 * @param mixed $settings
	 * @return void
	 */
	private function mobile_phone( $settings, $entry_id, $form_id ) {
		$cell_number  = isset( $settings['cell-number']  ) ? $settings['cell-number']  : '';
		$cell_carrier = isset( $settings['cell-carrier'] ) ? $settings['cell-carrier'] : '';
		$cell_message = isset( $settings['cell-message'] ) ? $settings['cell-message'] : '';

		// If no cell number, exit
		if ( empty( $cell_number ) )
			return;

		// If no cell carrier, exit
		if ( empty( $cell_carrier ) )
			return;

		// Remove most dashes, whitespace, and other characters
		$phone = trim( str_replace( array( '-', '(', ')', ',' ), '', $cell_number ) );

		$carrier = '';

		switch ( $cell_carrier ) {

			case 'Alltel' :
				$carrier = 'sms.alltelwireless.com';
				break;

			case 'ATT-iPhone' :
				$carrier = 'mms.att.net';
				break;

			case 'ATT' :
				$carrier = 'txt.att.net';
				break;

			case 'BellAtlantic' :
				$carrier = 'message.bam.com';
				break;

			case 'BellCanada' :
				$carrier = 'txt.bellmobility.ca';
				break;

			case 'BellMobilityCanada' :
				$carrier = 'txt.bell.ca';
				break;

			case 'BellMobility' :
				$carrier = 'txt.bellmobility.ca';
				break;

			case 'Boost' :
				$carrier = 'sms.myboostmobile.com';
				break;

			case 'CingularMe' :
				$carrier = 'cingularme.com';
				break;

			case 'Cingular' :
				$carrier = 'mobile.mycingular.com';
				break;

			case 'Nextel' :
				$carrier = 'messaging.nextel.com';
				break;

			case 'Rogers' :
				$carrier = 'sms.rogers.com';
				break;

			case 'Sprint' :
				$carrier = 'messaging.sprintpcs.com';
				break;

			case 'T-Mobile' :
				$carrier = 'tmomail.net';
				break;

			case 'Telus' :
				$carrier = 'msg.telus.com';
				break;

			case 'Verizon' :
				$carrier = 'vtext.com';
				break;

			case 'VirginMoblie' :
				$carrier = 'vmobl.com';
				break;

			case 'VirginMoblieCA' :
				$carrier = 'vmobile.ca';
				break;

			case 'Vodafone UK' :
				$carrier = 'vodafone.net';
				break;

			case 'Other' :
				$carrier = 'other';
				break;
		}

		// Make sure a carrier was assigned before proceeding
		if ( empty( $carrier ) )
			return;

		$txt      = 'other' == $carrier ? $phone : "{$phone}@{$carrier}";
		$subject  = sprintf( 'VFB Notification [#%d]', $entry_id );
		$message  = wp_strip_all_tags( $cell_message );

		wp_mail( $txt, $subject, $message );
	}

	/**
	 * mailchimp function.
	 *
	 * @access public
	 * @param mixed $settings
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function mailchimp( $settings, $entry_id, $form_id ) {
		$api          = isset( $settings['mailchimp-api']         ) ? $settings['mailchimp-api']         : '';
		$api_data     = isset( $settings['mailchimp-api-data']    ) ? $settings['mailchimp-api-data']    : '';
		$api_message  = isset( $settings['mailchimp-api-message'] ) ? $settings['mailchimp-api-message'] : '';
		$api_status   = isset( $settings['mailchimp-api-status']  ) ? $settings['mailchimp-api-status']  : '';

		$list          = isset( $settings['mailchimp-list']          ) ? $settings['mailchimp-list']          : '';
		$opt_in        = isset( $settings['mailchimp-opt-in']        ) ? $settings['mailchimp-opt-in']        : '';
		$merge_email   = isset( $settings['mailchimp-merge-email']   ) ? $settings['mailchimp-merge-email']   : '';
		$merge_fname   = isset( $settings['mailchimp-merge-fname']   ) ? $settings['mailchimp-merge-fname']   : '';
		$merge_lname   = isset( $settings['mailchimp-merge-lname']   ) ? $settings['mailchimp-merge-lname']   : '';
		$merge_company = isset( $settings['mailchimp-merge-company'] ) ? $settings['mailchimp-merge-company'] : '';
		$merge_phone   = isset( $settings['mailchimp-merge-phone']   ) ? $settings['mailchimp-merge-phone']   : '';

		// If no MailChimp API key, exit
		if ( empty( $api ) )
			return;

		// If no MailChimp list selected, exit
		if ( empty( $list ) )
			return;

		// If no email field selected, exit
		if ( empty( $merge_email ) )
			return;

		$merge_tags = $data = array();

		// Loop through entry data and set vars
		$meta = get_post_meta( $entry_id );
		foreach ( $meta as $key => $value ) {
			$field_id          = str_replace( '_vfb_field-', '', $key );
			$data[ $field_id ] = $value[0];
		}

		// Email Address
		if ( !empty( $merge_email ) && isset( $data[ $merge_email ] ) )
			$chimp_email = array( 'email' => $data[ $merge_email ] );

		// First Name
		if ( !empty( $merge_fname ) && isset( $data[ $merge_fname ] ) )
			$merge_tags['FNAME'] = $data[ $merge_fname ];

		// Last Name
		if ( !empty( $merge_lname ) && isset( $data[ $merge_lname ] ) )
			$merge_tags['LNAME'] = $data[ $merge_lname ];

		// Company
		if ( !empty( $merge_company ) && isset( $data[ $merge_company ] ) )
			$merge_tags['COMPANY'] = $data[ $merge_company ];

		// Phone
		if ( !empty( $merge_phone ) && isset( $data[ $merge_phone ] ) )
			$merge_tags['PHONE'] = $data[ $merge_phone ];

		list(, $datacentre) = explode( '-', $api );
		$api_endpoint = str_replace( '<dc>', $datacentre, 'https://<dc>.api.mailchimp.com/2.0/' );

		$args = array(
			'apikey'         => $api,
			'id'             => $list,
			'email'			 => $chimp_email,
			'merge_vars'     => $merge_tags,
			'double_optin'   => $opt_in,
		);

		$request = wp_remote_post( "$api_endpoint/lists/subscribe.json", array( 'body' => json_encode( $args ) ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	)
			return false;

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * campaign_monitor function.
	 *
	 * @access public
	 * @param mixed $settings
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function campaign_monitor( $settings, $entry_id, $form_id ) {
		$api         = isset( $settings['campaign-monitor-api']         ) ? $settings['campaign-monitor-api']         : '';
		$list        = isset( $settings['campaign-monitor-list-id']     ) ? $settings['campaign-monitor-list-id']     : '';
		$merge_email = isset( $settings['campaign-monitor-merge-email'] ) ? $settings['campaign-monitor-merge-email'] : '';
		$merge_fname = isset( $settings['campaign-monitor-merge-fname'] ) ? $settings['campaign-monitor-merge-fname'] : '';
		$merge_lname = isset( $settings['campaign-monitor-merge-lname'] ) ? $settings['campaign-monitor-merge-lname'] : '';

		// If no API key, exit
		if ( empty( $api ) )
			return;

		// If no email field selected, exit
		if ( empty( $merge_email ) )
			return;

		$merge_tags = $data = array();

		// Loop through entry data and set vars
		$meta = get_post_meta( $entry_id );
		foreach ( $meta as $key => $value ) {
			$field_id          = str_replace( '_vfb_field-', '', $key );
			$data[ $field_id ] = $value[0];
		}

		// Email Address
		if ( !empty( $merge_email ) && isset( $data[ $merge_email ] ) )
			$merge_tags['EMAIL'] = $data[ $merge_email ];

		// First Name
		if ( !empty( $merge_fname ) && isset( $data[ $merge_fname ] ) )
			$merge_tags['FNAME'] = $data[ $merge_fname ];

		// Last Name
		if ( !empty( $merge_lname ) && isset( $data[ $merge_lname ] ) )
			$merge_tags['LNAME'] = $data[ $merge_lname ];

		$name = '';
		if ( isset( $merge_tags['FNAME'] ) )
			$name = $merge_tags['FNAME'];
		if ( isset( $merge_tags['LNAME'] ) )
			$name .= ' ' . $merge_tags['LNAME'];

		$args = array(
			'EmailAddress' => $merge_tags['EMAIL'],
			'Name'         => $name,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_post( "https://api.createsend.com/api/v3.1/subscribers/{$list}.json", array( 'headers' => $headers, 'body' => json_encode( $args ) ) );

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	)
			return false;

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );
	}

	/**
	 * highrise function.
	 *
	 * @access public
	 * @param mixed $settings
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function highrise( $settings, $entry_id, $form_id ) {
		$api          = isset( $settings['highrise-api']         ) ? $settings['highrise-api']         : '';
		$api_data     = isset( $settings['highrise-api-data']    ) ? $settings['highrise-api-data']    : '';
		$api_message  = isset( $settings['highrise-api-message'] ) ? $settings['highrise-api-message'] : '';
		$api_status   = isset( $settings['highrise-api-status']  ) ? $settings['highrise-api-status']  : '';
		$subdomain    = isset( $settings['highrise-subdomain']   ) ? $settings['highrise-subdomain']   : '';

		$merge_email   = isset( $settings['highrise-merge-email']   ) ? $settings['highrise-merge-email']   : '';
		$merge_fname   = isset( $settings['highrise-merge-fname']   ) ? $settings['highrise-merge-fname']   : '';
		$merge_lname   = isset( $settings['highrise-merge-lname']   ) ? $settings['highrise-merge-lname']   : '';
		$merge_company = isset( $settings['highrise-merge-company'] ) ? $settings['highrise-merge-company'] : '';
		$merge_phone   = isset( $settings['highrise-merge-phone']   ) ? $settings['highrise-merge-phone']   : '';
		$note          = isset( $settings['highrise-note']          ) ? $settings['highrise-note']          : '';

		// If no API key, exit
		if ( empty( $api ) )
			return;

		// If no subdomain selected, exit
		if ( empty( $subdomain ) )
			return;

		// If no email field selected, exit
		if ( empty( $merge_email ) )
			return;

		$merge_tags = $data = array();

		// Loop through entry data and set vars
		$meta = get_post_meta( $entry_id );
		foreach ( $meta as $key => $value ) {
			$field_id          = str_replace( '_vfb_field-', '', $key );
			$data[ $field_id ] = $value[0];
		}

		// Email Address
		if ( !empty( $merge_email ) && isset( $data[ $merge_email ] ) )
			$merge_tags['EMAIL'] = $data[ $merge_email ];

		// First Name
		if ( !empty( $merge_fname ) && isset( $data[ $merge_fname ] ) )
			$merge_tags['FNAME'] = $data[ $merge_fname ];

		// Last Name
		if ( !empty( $merge_lname ) && isset( $data[ $merge_lname ] ) )
			$merge_tags['LNAME'] = $data[ $merge_lname ];

		// Company
		if ( !empty( $merge_company ) && isset( $data[ $merge_company ] ) )
			$merge_tags['COMPANY'] = $data[ $merge_company ];

		// Phone
		if ( !empty( $merge_phone ) && isset( $data[ $merge_phone ] ) )
			$merge_tags['PHONE'] = $data[ $merge_phone ];


		$contact = sprintf(
			'<person>
				<first-name>%1$s</first-name>
				<last-name>%2$s</last-name>
				<company-name>%3$s</company-name>
				<contact-data>
					<email-addresses>
						<email-address>
							<address>%4$s</address>
							<location>Work</location>
						</email-address>
					</email-addresses>
					<phone-numbers>
						<phone-number>
							<number>%5$s</number>
							<location>Work</location>
						</phone-number>
					</phone-numbers>
				</contact-data>
			</person>',
			$merge_tags['FNAME'],
			$merge_tags['LNAME'],
			$merge_tags['COMPANY'],
			$merge_tags['EMAIL'],
			$merge_tags['PHONE']
		);

		$args = array(
			'apikey'	=> $api,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api . ':nopass' ),
			'Content-Type' => 'application/xml',
		);

		$request = wp_remote_post( "https://{$subdomain}.highrisehq.com/people.xml", array( 'body' => $contact, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;

		// If no Notes, stop
		if ( empty( $note ) )
			return;

		// Retrieve and set response
		$response = simplexml_load_string( wp_remote_retrieve_body( $request ) );

		// Get the newly created Contact ID
		$person_id = !empty( $response->id ) ? (float) $response->id : false;

		// If no Contact ID, stop
		if ( !$person_id )
			return;

		$note = sprintf(
			'<note>
				<body>%1$s</body>
				<subject-id type="integer">%2$d</subject-id>
				<subject-type>Party</subject-type>
			</note>',
			$note,
			$person_id
		);

		$request = wp_remote_post( "https://{$subdomain}.highrisehq.com/notes.xml", array( 'body' => $note, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;
	}

	/**
	 * freshbooks function.
	 *
	 * @access public
	 * @param mixed $settings
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function freshbooks( $settings, $entry_id, $form_id ) {
		$api          = isset( $settings['freshbooks-api']         ) ? $settings['freshbooks-api']         : '';
		$api_data     = isset( $settings['freshbooks-api-data']    ) ? $settings['freshbooks-api-data']    : '';
		$api_message  = isset( $settings['freshbooks-api-message'] ) ? $settings['freshbooks-api-message'] : '';
		$api_status   = isset( $settings['freshbooks-api-status']  ) ? $settings['freshbooks-api-status']  : '';
		$subdomain    = isset( $settings['freshbooks-subdomain']   ) ? $settings['freshbooks-subdomain']   : '';

		$merge_email   = isset( $settings['freshbooks-merge-email']   ) ? $settings['freshbooks-merge-email']   : '';
		$merge_fname   = isset( $settings['freshbooks-merge-fname']   ) ? $settings['freshbooks-merge-fname']   : '';
		$merge_lname   = isset( $settings['freshbooks-merge-lname']   ) ? $settings['freshbooks-merge-lname']   : '';
		$merge_company = isset( $settings['freshbooks-merge-company'] ) ? $settings['freshbooks-merge-company'] : '';
		$merge_phone   = isset( $settings['freshbooks-merge-phone']   ) ? $settings['freshbooks-merge-phone']   : '';
		$note          = isset( $settings['freshbooks-note']          ) ? $settings['freshbooks-note']          : '';

		// If no API key, exit
		if ( empty( $api ) )
			return;

		// If no subdomain selected, exit
		if ( empty( $subdomain ) )
			return;

		// If no email field selected, exit
		if ( empty( $merge_email ) )
			return;

		$merge_tags = $data = array();

		// Loop through entry data and set vars
		$meta = get_post_meta( $entry_id );
		foreach ( $meta as $key => $value ) {
			$field_id          = str_replace( '_vfb_field-', '', $key );
			$data[ $field_id ] = $value[0];
		}

		// Email Address
		if ( !empty( $merge_email ) && isset( $data[ $merge_email ] ) )
			$merge_tags['EMAIL'] = $data[ $merge_email ];

		// First Name
		if ( !empty( $merge_fname ) && isset( $data[ $merge_fname ] ) )
			$merge_tags['FNAME'] = $data[ $merge_fname ];

		// Last Name
		if ( !empty( $merge_lname ) && isset( $data[ $merge_lname ] ) )
			$merge_tags['LNAME'] = $data[ $merge_lname ];

		// Company
		if ( !empty( $merge_company ) && isset( $data[ $merge_company ] ) )
			$merge_tags['COMPANY'] = $data[ $merge_company ];

		// Phone
		if ( !empty( $merge_phone ) && isset( $data[ $merge_phone ] ) )
			$merge_tags['PHONE'] = $data[ $merge_phone ];

		$client = sprintf(
			'<?xml version="1.0" encoding="utf-8"?>
			<request method="client.create">
				<client>
					<first_name>%1$s</first_name>
				    <last_name>%2$s</last_name>
				    <organization>%3$s</organization>
				    <email>%4$s</email>
				    <work_phone>%5$s</work_phone>
				    <notes>%6$s</notes>
				</client>
			</request>',
			$merge_tags['FNAME'],
			$merge_tags['LNAME'],
			$merge_tags['COMPANY'],
			$merge_tags['EMAIL'],
			$merge_tags['PHONE'],
			$note
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api . ':nopass' ),
			'Content-Type'  => 'application/xml',
		);

		$request = wp_remote_post( "https://{$subdomain}.freshbooks.com/api/2.1/xml-in", array( 'body' => $client, 'headers' => $headers ) );

		// If request fails, stop
		if ( is_wp_error( $request ) )
			return false;
	}
}
