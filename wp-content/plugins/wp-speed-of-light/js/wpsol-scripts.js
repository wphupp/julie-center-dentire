jQuery(document).ready(function($){
    function wpsol_cache_callAjax(){
        jQuery.ajax({
            url:ajaxurl,
            dataType:'json',
            method:'POST',
            data:{
                action:'wpsol_ajax_clean_cache'
            },
            success : function(res){
                current = location.href;
                res = parseFloat(res) ;
                if(current.indexOf("page=wpsol_parameters") > 0){
                    window.location.href = current+ "#msg=success-cleancache&file="+res;
                }else{
                    window.location.href = current+ "msg=success-cleancache&file="+res;
                }
                location.reload();
            }
        });
    }
    var url = location.href;
    var fileClean = parseFloat(getParameterByName('file',url) );

    $( window ).load(function() {
        var patt = /wp-admin/i;
        var div = '<div id="message" class="notice notice-success" style=" margin-top:10px; margin-bottom:10px ;padding: 10px;"><strong>OK cache is clean: '+fileClean+'Kb cleaned</strong></div>';
        if(patt.test(url)){
            //backend
            if(url.indexOf("msg=success-cleancache") > 0 && !isNaN(fileClean) ) {
                $("#wpbody").prepend(div);
            }
        }else{
            //frontend
        }

    });



    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    jQuery('#clean-cache-button').click(function(){
        wpsol_cache_callAjax();
    });

    jQuery('#wp-admin-bar-wpsol-clean-cache-topbar').click(function(){
        wpsol_cache_callAjax();
    });
});