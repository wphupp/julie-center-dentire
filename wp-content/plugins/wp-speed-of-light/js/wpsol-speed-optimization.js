jQuery(document).ready(function($){
    jQuery('.speedoflight_tool').qtip({
        content: {
            attr: 'alt'
        },
        position: {
            my: 'bottom left',
            at: 'top top'
        },
        style: {
            tip: {
                corner: true,
            },
            classes: 'speedoflight-qtip qtip-rounded speedoflight-qtip-dashboard'
        },
        show: 'hover',
        hide: {
            fixed: true,
            delay: 10
        }

    }); 
    // change value 
    $(".wpsol-optimization").change(function(){
        var val = $("#clean-cache").val();
        ($("#active-cache").is(':checked')) ? $("#active-cache").attr("value","1") : $("#active-cache").attr("value","0");
        ($("#active-compression").is(':checked')) ? $("#active-compression").attr("value","1") : $("#active-compression").attr("value","0");
        ($("#add-expires").is(':checked')) ? $("#add-expires").attr("value","1") : $("#add-expires").attr("value","0");    
        $("#clean-cache").attr("value",val);
    });
     // change value 
    $(".wpsol-minification").change(function(){
        ($("#html-minification").is(':checked')) ? $("#html-minification").attr("value","1") : $("#html-minification").attr("value","0");
        ($("#css-minification").is(':checked')) ? $("#css-minification").attr("value","1") : $("#css-minification").attr("value","0");
        ($("#js-minification").is(':checked')) ? $("#js-minification").attr("value","1") : $("#js-minification").attr("value","0");
        ($("#cssGroup-minification").is(':checked')) ? $("#cssGroup-minification").attr("value","1") : $("#cssGroup-minification").attr("value","0");
        ($("#jsGroup-minification").is(':checked')) ? $("#jsGroup-minification").attr("value","1") : $("#jsGroup-minification").attr("value","0");
    });    
    
});