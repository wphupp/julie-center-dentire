<?php
//Based on some work of simple-cache
if ( ! defined( 'ABSPATH' ) ) exit;
class  WpSoL_Cache{
    
    public function __construct() {
        
    }
    public function set_action(){
        
    }
    
    //create advanced-cache file
    public function write(){
        global $wp_filesystem;

		$file = untrailingslashit( WP_CONTENT_DIR )  . '/advanced-cache.php';

		$config = get_option('wpsol_optimization_settings');

		$file_string = '';

		if (!empty($config) && ! empty( $config['speed_optimization']['act_cache'] ) ) {

			$file_string = '<?php ' .
                        "\n\r" . "defined( 'ABSPATH' ) || exit;" .
			"\n\r" . "define( 'WPSOL_ADVANCED_CACHE', true );" .
			"\n\r" . 'if ( is_admin() ) { return; }' .
            "\n\r" . "if ( ! @file_exists( WP_CONTENT_DIR . '/plugins/wp-speed-of-light/wp-speed-of-light.php' ) ) { return; }" .
			"\n\r" . "if ( ! @file_exists( WP_CONTENT_DIR . '/wpsol-config/config-' . \$_SERVER['HTTP_HOST'] . '.php' ) ) { return; }" .
			"\n\r" . "\$GLOBALS['wpsol_config'] = include( WP_CONTENT_DIR . '/wpsol-config/config-' . \$_SERVER['HTTP_HOST'] . '.php' );" .
			"\n\r" . "if ( empty( \$GLOBALS['wpsol_config'] ) || empty( \$GLOBALS['wpsol_config']['speed_optimization']['act_cache'] ) ) { return; }" .
			"\n\r" . "if ( @file_exists(  WP_CONTENT_DIR . '/plugins/wp-speed-of-light/inc/caches/file-page-cache.php' ) ) { include_once( WP_CONTENT_DIR  . '/plugins/wp-speed-of-light/inc/caches/file-page-cache.php' ); }" . "\n\r";

		}

		if ( ! $wp_filesystem->put_contents( $file, $file_string ) ) {
			return false;
		}

		return true;
    }
//    create file config
    public static function write_config( $config ) {
                
		global $wp_filesystem;

		$config_dir = WP_CONTENT_DIR  . '/wpsol-config';
                
		$site_url_parts = parse_url( site_url() );

		$config_file = $config_dir  . '/config-' . $site_url_parts['host'] . '.php';

		$wp_filesystem->mkdir( $config_dir );

		$config_file_string = '<?php ' . "\n\r" . "defined( 'ABSPATH' ) || exit;" . "\n\r" . 'return ' . var_export( $config, true ) . '; ' . "\n\r";

		if ( ! $wp_filesystem->put_contents( $config_file, $config_file_string ) ) {
			return false;
		}

		return true;
	}
    //turn on / of wp cahe
    public function toggle_caching( $status ) {

		global $wp_filesystem;
		if ( defined( 'WP_CACHE' ) && WP_CACHE === $status ) {
			return;
		}

		// Lets look 4 levels deep for wp-config.php
		$levels = 4;

		$file = '/wp-config.php';
		$config_path = false;

		for ( $i = 1; $i <= 3; $i++ ) {
			if ( $i > 1 ) {
				$file = '/..' . $file;
			}

			if ( $wp_filesystem->exists( untrailingslashit( ABSPATH )  . $file ) ) {
				$config_path = untrailingslashit( ABSPATH )  . $file;
				break;
			}
		}

		// Couldn't find wp-config.php
		if ( ! $config_path ) {
			return false;
		}

		$config_file_string = $wp_filesystem->get_contents( $config_path );

		// Config file is empty. Maybe couldn't read it?
		if ( empty( $config_file_string ) ) {
			return false;
		}

		$config_file = preg_split( "#(\n|\r)#", $config_file_string );
		$line_key = false;

		foreach ( $config_file as $key => $line ) {
			if ( ! preg_match( '/^\s*define\(\s*(\'|")([A-Z_]+)(\'|")(.*)/', $line, $match ) ) {
				continue;
			}

			if ( $match[2] == 'WP_CACHE' ) {
				$line_key = $key;
			}
		}

		if ( $line_key !== false ) {
			unset( $config_file[ $line_key ] );
		}

		$status_string = ( $status ) ? 'true' : 'false';

		array_shift( $config_file );
		array_unshift( $config_file, '<?php', "define( 'WP_CACHE', $status_string ); " );

		foreach ( $config_file as $key => $line ) {
			if ( '' === $line ) {
				unset( $config_file[$key] );
			}
		}

		if ( ! $wp_filesystem->put_contents( $config_path, implode( PHP_EOL, $config_file ) ) ) {
			return false;
		}

		return true;
	}
     //clean cache   
    public static function wpsol_cache_flush() {
	global $wp_filesystem;

        require_once( ABSPATH . 'wp-admin/includes/file.php');

	WP_Filesystem();

	$wp_filesystem->rmdir( untrailingslashit( WP_CONTENT_DIR ) . '/cache/wpsol-cache', true );

	if ( function_exists( 'wp_cache_flush' ) ) {
		wp_cache_flush();
	}
    }
    
    //delete file for clean up
    
    public function clean_up() {

		global $wp_filesystem;
		$file = untrailingslashit( WP_CONTENT_DIR )  . '/advanced-cache.php';

		$ret = true;

		if ( ! $wp_filesystem->delete( $file ) ) {
			$ret = false;
		}

		$folder = untrailingslashit( WP_CONTENT_DIR )  . '/cache/wpsol-cache';

		if ( ! $wp_filesystem->delete( $folder, true ) ) {
			$ret = false;
		}

		return $ret;
	}
   //delete config file
    public function clean_config() {

		global $wp_filesystem;

		$folder = untrailingslashit( WP_CONTENT_DIR )  . '/wpsol-config';
		if ( ! $wp_filesystem->delete( $folder, true ) ) {
			return false;
		}

		return true;
	}

    /**
     * Function write config to wpsol-config
     * @return WpSoL_Cache
     */
    public static function write_config_cache(){
        $ecommerce_exclude_urls = array();
        $optimization = get_option('wpsol_optimization_settings');
        $disable_for_adminuser = get_option('wpsol_disable_optimizition_by_role');
        if ( empty($optimization)){
            $optimization = array(
                'speed_optimization' => array(
                    'act_cache' => 1,
                    'clean_cache'=>40,
                    'devices' => array(
                        'cache_desktop' =>  1,
                        'cache_tablet' =>  1,
                        'cache_mobile' =>  1,
                    ),
                    'act_compression'=>1,
                    'add_expires' => 1
                )
            );
        }
        $config['speed_optimization'] = $optimization['speed_optimization'];

        //get parameter config
        $opt_config = get_option('wpsol_configuration');
        if ( empty($opt_config)){
            $opt_config['disable_page'] = array();
        }
        if( class_exists('WooCommerce')){
            $ecommerce_exclude_urls = WpSoL_EcommerceCache::factory()->wpsol_ecommerce_exclude_pages();
        }
        $config['disable_page'] = array_merge($ecommerce_exclude_urls,$opt_config['disable_page']);
        $config['homepage'] = get_site_url();
        $config['disable_per_adminuser'] = 0;
        if(!empty($disable_for_adminuser)){
            $config['disable_per_adminuser'] = $disable_for_adminuser['disable'];
        }
        if(! self::write_config($config)){
             return false;
        }
        return true;
    }

    public static function factory() {

		static $instance;

		if ( ! $instance ) {
			$instance = new self();
			$instance->set_action();
		}

		return $instance;
	}
}
