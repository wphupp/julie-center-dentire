<?php
if ( ! defined( 'ABSPATH' ) ) exit;
//Main plugin functions here
class WpSoL_Admin {

    function __construct() {
        add_action('admin_menu', array($this, 'register_menu_page'));
        /** Load admin js * */
        add_action('admin_enqueue_scripts', array($this, 'loadAdminScripts'));
        // load admin css //
        add_action('admin_init', array($this, 'addAdminStylesheets'));
        //** load languages **//
        add_action('init', function(){
            load_plugin_textdomain('wp-speed-of-light', false, dirname(plugin_basename(WPSOL_FILE)) . '/languages/');
        });

        $this->ajaxHandle();
    }

    function register_menu_page() {
        // add main menu
        $admin_page = add_menu_page(__('WP Speed of Light:', 'wp-speed-of-light') . ' ' . __('Dashboard', 'wp-speed-of-light'), __('WP Speed of Light', 'wp-speed-of-light'), 'manage_options', 'wpsol_dashboard', array(
            $this, 'load_page',), 'dashicons-performance');

        $manage_options_cap = apply_filters('wpsol_manage_options_capability', 'manage_options');
        // add submenu
        $submenu_pages = array(
            array(
                'wpsol_dashboard',
                '',
                __('Speed analysis', 'wp-speed-of-light'),
                $manage_options_cap,
                'wpsol_speed_analysis',
                array($this, 'load_page'),
                null,
            ),
            array(
                'wpsol_dashboard',
                '',
                __('Speed optimization', 'wp-speed-of-light'),
                $manage_options_cap,
                'wpsol_speed_optimization',
                array($this, 'load_page'),
                null,
            ),
            array(
                'wpsol_dashboard',
                '',
                __('Image Compression', 'wp-speed-of-light'),
                $manage_options_cap,
                'wpsol_image_compression',
                array($this, 'load_page'),
                null,
            ),
            array(
                'wpsol_dashboard',
                '',
                __('Database cleanup', 'wp-speed-of-light'),
                $manage_options_cap,
                'wpsol_database_cleanup',
                array($this, 'load_page'),
                null,
            ),
            array(
                'wpsol_dashboard',
                '',
                __('Configuration', 'wp-speed-of-light'),
                $manage_options_cap,
                'wpsol_parameters',
                array($this, 'load_page'),
                null,
            ),
        );

        if (count($submenu_pages)) {
            foreach ($submenu_pages as $submenu_page) {
                // Add submenu page
                $admin_page = add_submenu_page($submenu_page[0], $submenu_page[2] . ' - ' . __('WP Speed of Light:', 'wp-speed-of-light'), $submenu_page[2], $submenu_page[3], $submenu_page[4], $submenu_page[5]);
            }
        }
        //change name dashboard
        global $submenu;
        if (isset($submenu['wpsol_dashboard']) && current_user_can($manage_options_cap)) {
            $submenu['wpsol_dashboard'][0][0] = __('Dashboard', 'wp-speed-of-light');
        }
    }

    function load_page() {
        if (isset($_GET['page'])) {
            switch ($_GET['page']) {
                case 'wpsol_speed_analysis':
                    require_once( WPSOL_PLUGIN_DIR . 'inc/pages/speed_analysis.php' );
                    break;
                case 'wpsol_speed_optimization':
                    require_once( WPSOL_PLUGIN_DIR . 'inc/pages/speed_optimization.php' );
                    break;
                case 'wpsol_image_compression':
                    require_once( WPSOL_PLUGIN_DIR . 'inc/pages/image_compression.php' );
                    break;
                case 'wpsol_database_cleanup':
                    require_once( WPSOL_PLUGIN_DIR . 'inc/pages/database_cleanup.php' );
                    break;
                case 'wpsol_parameters':
                    require_once (WPSOL_PLUGIN_DIR . 'inc/pages/configuration.php');
                    break;

                case 'wpsol_dashboard':
                default:
                    require_once( WPSOL_PLUGIN_DIR . 'inc/pages/dashboard.php' );
                    break;
            }
        }
    }

    function loadAdminScripts() {

        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-dialog');
        wp_enqueue_script('jquery-ui-progressbar');
        wp_enqueue_script(
                'wpsol-admin-js-speed', plugins_url('js/wpsol-admin.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true
        );
        wp_localize_script('wpsol_Admin', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
       
        $current_screen = get_current_screen();
        if ($current_screen->base == 'toplevel_page_wpsol_dashboard') {
            wp_enqueue_style('wpsol-quirk', plugins_url('/css/quirk.css', dirname(__FILE__)));
            wp_enqueue_style('wpsol-ddashboard', plugins_url('/css/dashboard.css', dirname(__FILE__)));
            wp_enqueue_script('wpsol-dashboard', plugins_url('js/dashboard.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-tooltip', plugins_url('js/material/tooltip.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-global', plugins_url('js/material/global.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-velocity', plugins_url('js/material/velocity.min.js', dirname(__FILE__)), array('jquery'), '0.1', true);
        }

        if ($current_screen->base == 'wp-speed-of-light_page_wpsol_speed_analysis') {
            wp_enqueue_script('wpsol-canvasjs', plugins_url('js/canvasjs.min.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-speed_analysis', plugins_url('js/wpsol-speed-analysis.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-speed_tablesorter', plugins_url('js/jquery.tablesorter.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_style('wpsol-speed_analysis', plugins_url('/css/speed_analysis.css', dirname(__FILE__)));
            wp_enqueue_style('wpsol-font_awesome', plugins_url('/css/fonts/font-awesome.min.css', dirname(__FILE__)));
            wp_enqueue_style('style-light-speed-jquery-ui-fresh');
            wp_enqueue_script('wpsol-velocity', plugins_url('js/material/velocity.min.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-waves', plugins_url('js/material/waves.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-tooltip', plugins_url('js/material/tooltip.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-global', plugins_url('js/material/global.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script(
               'wpsol-speed_cookie', plugins_url('js/jquery.cookie.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true
             );
            
        }

        if ($current_screen->base == 'wp-speed-of-light_page_wpsol_speed_optimization') {
            wp_enqueue_style('wpsol-quirk', plugins_url('/css/quirk.css', dirname(__FILE__)));
            wp_enqueue_style('wpsol-speed_optimization', plugins_url('/css/speed_optimization.css', dirname(__FILE__)));
            wp_enqueue_script('wpsol-speed-optimization', plugins_url('js/wpsol-speed-optimization.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_style('style-light-speed-jquery-ui-fresh');
            wp_enqueue_script('wpsol-material_tabs', plugins_url('js/material/tabs.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-velocity', plugins_url('js/material/velocity.min.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script('wpsol-waves', plugins_url('js/material/waves.js', dirname(__FILE__)), array('jquery'), '0.1', true);
            wp_enqueue_script(
               'wpsol-speed_cookie', plugins_url('js/jquery.cookie.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true
            );
        }
        if ($current_screen->base == 'wp-speed-of-light_page_wpsol_database_cleanup') {
            wp_enqueue_style('wpsol-database_cleanup', plugins_url('/css/database_cleanup.css', dirname(__FILE__)));
            wp_enqueue_script('wpsol-database-cleanup', plugins_url('js/wpsol-database-cleanup.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-waves', plugins_url('js/material/waves.js', dirname(__FILE__)), array('jquery'), '0.1', true);
        }
        if ($current_screen->base == 'wp-speed-of-light_page_wpsol_parameters') {
            wp_enqueue_script('wpsol-configuration-js', plugins_url('js/wpsol-configuration.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_style('wpsol-configuration', plugins_url('/css/configuration.css', dirname(__FILE__)));
            wp_enqueue_script('wpsol-waves', plugins_url('js/material/waves.js', dirname(__FILE__)), array('jquery'), '0.1', true);

            wp_enqueue_style('wpsol-speed_optimization', plugins_url('/css/speed_optimization.css', dirname(__FILE__)));
            wp_enqueue_script('wpsol-material_tabs', plugins_url('js/material/tabs.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true);
            wp_enqueue_script('wpsol-velocity', plugins_url('js/material/velocity.min.js', dirname(__FILE__)), array('jquery'), '0.1', true);

        }

        if ($current_screen->base == 'wp-speed-of-light_page_wpsol_speed_optimization' || $current_screen->base == 'wp-speed-of-light_page_wpsol_database_cleanup' || $current_screen->base == 'wp-speed-of-light_page_wpsol_parameters') {
            wp_enqueue_style('jquery-qtip', plugins_url('css/jquery.qtip.css', dirname(__FILE__)), array(), WPSOL_VERSION);
            wp_enqueue_script('jquery-qtip', plugins_url('js/jquery.qtip.min.js', dirname(__FILE__)), array('jquery'), '2.2.1', true);
        }
    }

    function addAdminStylesheets() {
        wp_register_style('style-light-speed-jquery-ui-fresh', plugins_url('css/jquery-ui-fresh.css', dirname(__FILE__)), array(), WPSOL_VERSION);
    }

    function ajaxHandle() {
        add_action('wp_ajax_activate_button', array($this, 'activate_button'));
        add_action('wp_ajax_wpsol_activate_cache_link', array($this, 'wpsol_activate_cache_link'));
        add_action('wp_ajax_wpsol_load_page_time',array('WpSoL_SpeedAnalysis','load_page_time'));
        add_action('wp_ajax_wpsol_start_scan_query', array('WpSoL_SpeedAnalysis', 'start_scan_query'));
        add_action('wp_ajax_wpsol_stop_scan_query', array('WpSoL_SpeedAnalysis', 'stop_scan_query'));
        add_action('wp_ajax_wpsol_ajax_clean_cache', array('WpSoL_Configuration', 'ajax_clean_cache'));
        add_action('wp_ajax_wpsol_more_details', array('WpSoL_SpeedAnalysis', 'more_details'));
        add_action('wp_ajax_wpsol_delete_details', array('WpSoL_SpeedAnalysis', 'delete_details'));
    }

    function activate_button() {
        $act = get_option('wpsol_plugin_install');
        if(empty($act)){
            $act = array(
                'active_button' => '',
                'active_cache' => ''
            );
        }
        if(isset($_POST['datas']))
        $act['active_button'] = $_POST['datas'];

        update_option('wpsol_plugin_install', $act);

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* special ajax here */
            wp_die();
        }
    }

    function wpsol_activate_cache_link(){
        $act = get_option('wpsol_plugin_install');

        if(empty($act)){
            $act = array(
                'active_button' => '',
                'active_cache' => ''
            );
        }

        if(isset($_POST['active_cache']))
            $act['active_cache'] = $_POST['active_cache'];

        update_option('wpsol_plugin_install', $act);

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* special ajax here */
            wp_die();
        }
    }
}
