<?php
if ( ! defined( 'ABSPATH' ) ) exit;
//Main plugin functions here
class WpSoLMain {

    private static $initiated = false;

    public function __construct() {
        add_action('admin_notices', array($this, 'plugin_activation_notices'));
        //Update option when update plugin
        add_action('admin_init', array($this, 'wpsol_update_version'));
    }
    /**
     * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
     * @static
     */
    public static function plugin_activation() {
        WP_Filesystem();
        $opts = get_option('wpsol_optimization_settings');
        $default_opts = array(
            'speed_optimization' => array(
                'act_cache' => 1,
                'clean_cache'=> 40,
                'act_compression'=>1,
                'add_expires' => 1,
                'devices' => array(
                    'cache_desktop' =>  1,
                    'cache_tablet' =>  1,
                    'cache_mobile' =>  1,
                ),
            ),
            'advanced_features' => array(
                'html_minification' => 0,
                'css_minification' => 0,
                'js_minification' => 0,
                'cssgroup_minification' => 0,
                'jsgroup_minification' => 0
            )
        );

        $opts["speed_optimization"] = array_merge($default_opts["speed_optimization"],$opts["speed_optimization"] );
        $opts["advanced_features"]  = array_merge($default_opts["advanced_features"],$opts["advanced_features"] );

        update_option('wpsol_optimization_settings', $opts);
        //add header to htaccess by default
        WpSoL_SpeedOptimization::add_expires_header(TRUE);
        WpSoL_SpeedOptimization::add_gzip_htacess(TRUE);
        //automatic config start cache
        WpSoL_Cache::factory()->write();
        WpSoL_Cache::factory()->write_config_cache();

        if ( !empty($opts) && !empty($opts['speed_optimization']['act_cache'] )) {
                WpSoL_Cache::factory()->toggle_caching( true );
        } 
        //diable optimizition by role
        $check = array('disable' => 0);
        update_option('wpsol_disable_optimizition_by_role', $check);

        //config by default
        $config = get_option('wpsol_configuration');
        if(empty($config)){
            $config = array(
                'disable_user' => 0,
                'display_clean' => 1,
                'disable_page' => array(),
                'webtest_api_key' => ''
            );
        }
        $config['display_clean'] = 1;
        update_option('wpsol_configuration',$config);
        //display message plugin active
        if (version_compare($GLOBALS['wp_version'], WPSOL_MINIMUM_WP_VERSION, '<')) {
            deactivate_plugins(basename(__FILE__));
            wp_die('<p>The <strong>WP Speed of Light</strong> plugin requires WordPress ' . WPSOL_MINIMUM_WP_VERSION . ' or higher.</p>', 'Plugin Activation Error', array('response' => 200, 'back_link' => TRUE));
        }
       
    }

    /**
     * Removes all connection options
     * @static
     */
    public static function plugin_deactivation() {
        WP_Filesystem();
        
        WpSoL_Cache::factory()->clean_up();
        WpSoL_Cache::factory()->toggle_caching(false);
        WpSoL_Cache::factory()->clean_config();
        
        WpSoL_MinificationCache::clear_minification();
        //delete header in htacctess
        WpSoL_SpeedOptimization::add_expires_header(FALSE);
        WpSoL_SpeedOptimization::add_gzip_htacess(FALSE);
        
    }

    public static function plugin_activation_notices() {
        $active = get_option('wpsol_plugin_install');
        $pages = get_current_screen();
        if($pages->base == 'plugins'){
            if (empty($active['active_button'])) {
                ?>
                <div class="notice notice-warning ">
                    <p>
                <?php _e('Nice! the plugin is installed. Your WordPress will approach the speed of light', 'wp-speed-of-light'); ?>
                        <a href="admin.php?page=wpsol_dashboard" class="button button-primary" id="wpsol_activate" style="margin-left: 5px;"><?php _e('Activate and configure now', 'wp-speed-of-light'); ?></a>
                    </p>
                </div>
                <?php
            }

            if (empty($active['active_cache'])) {
                ?>
                <div class="notice notice-success ">
                    <p>
                        <?php _e('The cache has been enabled by default. You can deactivate it in ', 'wp-speed-of-light'); ?>
                        <a href="" class="" id="wpsol_activate_cache_link" ><?php _e('the preferences page', 'wp-speed-of-light'); ?></a>
                    </p>
                </div>
                <?php
            }

        }
    }

    public function wpsol_update_version(){
        global $wpdb;
        $option_ver = 'wpsol_db_version1.3.0';
        $db_installed = get_option($option_ver, false);

        if (!$db_installed) {
            // update option wpsol_optimization_settings
            $opts = get_option('wpsol_optimization_settings');
            $default_opts = array(
                'speed_optimization' => array(
                    'act_cache' => 1,
                    'clean_cache'=> 40,
                    'act_compression'=>1,
                    'add_expires' => 1,
                    'devices' => array(
                        'cache_desktop' =>  1,
                        'cache_tablet' =>  1,
                        'cache_mobile' =>  1,
                    ),
                ),
                'advanced_features' => array(
                    'html_minification' => 0,
                    'css_minification' => 0,
                    'js_minification' => 0,
                    'cssgroup_minification' => 0,
                    'jsgroup_minification' => 0
                )
            );

            $opts["speed_optimization"] = array_merge($default_opts["speed_optimization"],$opts["speed_optimization"] );
            $opts["advanced_features"]  = array_merge($default_opts["advanced_features"],$opts["advanced_features"] );

            update_option('wpsol_optimization_settings', $opts);
            update_option($option_ver, true);
        }
    }

}