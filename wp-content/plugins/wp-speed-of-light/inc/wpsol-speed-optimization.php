<?php
if ( ! defined( 'ABSPATH' ) ) exit;
class WpSoL_SpeedOptimization {

    public $cache;
    public $minification;

    public function __construct() {
        $this->cache = array('speed_optimization' => array());
        $this->minification = array('advanced_features' => array());
    }

    public function set_action() {

        add_action('load-wp-speed-of-light_page_wpsol_speed_optimization', array($this, 'save_settings'));
        add_action('pre_post_update', array($this, 'purge_post_on_update'), 10, 1);
        add_action('save_post', array($this, 'purge_post_on_update'), 10, 1);
        add_action('wp_trash_post', array($this, 'purge_post_on_update'), 10, 1);
        add_action('comment_post', array($this, 'purge_post_on_new_comment'), 10, 3);
        add_action('wp_set_comment_status', array($this, 'purge_post_on_comment_status_change'), 10, 2);
        add_action('set_comment_cookies', array($this, 'set_comment_cookie_exceptions'), 10, 2);
    }

    /**
     * When user posts a comment, set a cookie so we don't show them page cache
     *
     * @param  WP_Comment $comment
     * @param  WP_User $user
     * @since  1.3
     */
    public function set_comment_cookie_exceptions($comment, $user) {
        $config = get_option('wpsol_optimization_settings');
        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {

            $post_id = $comment->comment_post_ID;

            setcookie('wpsol_commented_posts[' . $post_id . ']', parse_url(get_permalink($post_id), PHP_URL_PATH), ( time() + HOUR_IN_SECONDS * 24 * 30));
        }
    }

    /**
     * action when save settings optimization
     */
    public function save_settings() {
        $opts = get_option('wpsol_optimization_settings');
        //save setting speed optimization
        if (!empty($_REQUEST['action']) && 'wpsol_save_settings' === $_REQUEST['action']) {
            check_admin_referer('wpsol_speed_optimization','_wpsol_nonce');
            $this->save_settings_speed_optimization($opts);
            WP_Filesystem();

            WpSoL_Cache::factory()->write();
            //write config for cache
            WpSoL_Cache::factory()->write_config_cache();

            if (!empty($this->cache) && !empty($this->cache['speed_optimization']['act_cache'])) {
                WpSoL_Cache::factory()->toggle_caching(true);
            } else {
                WpSoL_Cache::factory()->toggle_caching(false);
            }
            // Reschedule cron events
            WpSoL_CleanCacheTime::factory()->unschedule_events();
            WpSoL_CleanCacheTime::factory()->schedule_events();
            //add expire header
            if (!empty($this->cache['speed_optimization']['add_expires'])) {
                $this->add_expires_header(TRUE);
            } else {
                $this->add_expires_header(FALSE);
            }

            //add gzip to .htaccess
            if( !empty($this->cache['speed_optimization']['act_compression'])){
                $this->add_gzip_htacess(TRUE);
            }else{
                $this->add_gzip_htacess(FALSE);
            }
            //clear cache after save settings
            WpSoL_Cache::wpsol_cache_flush();
            WpSoL_MinificationCache::clear_minification();


        }
        //save settings on advanced optimization page
        if (!empty($_REQUEST['action']) && 'wpsol_save_minification' === $_REQUEST['action']) {
            check_admin_referer('wpsol_advanced_optimization','_wpsol_nonce');
            $this->save_settings_advanced_features($opts);
            //clear minification
            WpSoL_Cache::wpsol_cache_flush();
            WpSoL_MinificationCache::clear_minification();
        }
        //return current page
        if (!empty($_REQUEST['_wp_http_referer'])) {
            wp_redirect($_REQUEST['_wp_http_referer'].'&save-settings=success');
            exit;
        }
    }

//    Automatically purge all file based page cache on post changes
    public function purge_post_on_update($post_id) {
        $post_type = get_post_type($post_id);
        if (( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || 'revision' === $post_type) {
            return;
        } elseif (!current_user_can('edit_post', $post_id) && (!defined('DOING_CRON') || !DOING_CRON )) {
            return;
        }

        $config = get_option('wpsol_optimization_settings');

        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            WpSoL_Cache::wpsol_cache_flush();
        }
    }

    public function purge_post_on_new_comment($comment_ID, $approved, $commentdata) {
        if (empty($approved)) {
            return;
        }
        $config = get_option('wpsol_optimization_settings');
        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            $post_id = $commentdata['comment_post_ID'];

            global $wp_filesystem;

            if ( empty( $wp_filesystem ) ) {
                require_once( ABSPATH . '/wp-admin/includes/file.php' );
                WP_Filesystem();
            }

            $url_path =  get_permalink($post_id);
            if ( $wp_filesystem->exists( untrailingslashit( WP_CONTENT_DIR ) . '/cache/wpsol-cache/' .  md5($url_path) ) ) {
                $wp_filesystem->rmdir(  untrailingslashit( WP_CONTENT_DIR ) . '/cache/wpsol-cache/' .  md5($url_path), true );
            }
        }
    }

//            if a comments status changes, purge it's parent posts cache
    public function purge_post_on_comment_status_change($comment_ID, $comment_status) {
        $config = get_option('wpsol_optimization_settings');

        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            $comment = get_comment($comment_ID);
            $post_id = $comment->comment_post_ID;

            global $wp_filesystem;

            WP_Filesystem();

            $url_path =  get_permalink($post_id);

            if ( $wp_filesystem->exists( untrailingslashit( WP_CONTENT_DIR ) . '/cache/wpsol-cache/' .  md5($url_path) ) ) {
                $wp_filesystem->rmdir(  untrailingslashit( WP_CONTENT_DIR ) . '/cache/wpsol-cache/' .  md5($url_path), true );
            }

        }
    }

    // save settings physical   
    public function save_settings_speed_optimization($opts) {
        if (!empty($opts)) {
            if (isset($_REQUEST['active-cache'])) {
                $this->cache['speed_optimization']['act_cache'] = (int) $_REQUEST['active-cache'];
            } else {
                $this->cache['speed_optimization']['act_cache'] = 0;
            }
            if (isset($_REQUEST['clean-cache'])) {
                $this->cache['speed_optimization']['clean_cache'] = (int) $_REQUEST['clean-cache'];
            } else {
                $this->cache['speed_optimization']['clean_cache'] = 0;
            }
            if (isset($_REQUEST['cache-desktop'])) {
                $this->cache['speed_optimization']['devices']['cache_desktop'] = (int) $_REQUEST['cache-desktop'];
            }
            if (isset($_REQUEST['cache-tablet'])) {
                $this->cache['speed_optimization']['devices']['cache_tablet'] = (int) $_REQUEST['cache-tablet'];
            }
            if (isset($_REQUEST['cache-mobile'])) {
                $this->cache['speed_optimization']['devices']['cache_mobile'] = (int) $_REQUEST['cache-mobile'];
            }
            if (isset($_REQUEST['active-compression'])) {
                $this->cache['speed_optimization']['act_compression'] = (int) $_REQUEST['active-compression'];
            } else {
                $this->cache['speed_optimization']['act_compression'] = 0;
            }
            if (isset($_REQUEST['add-expires'])) {
                $this->cache['speed_optimization']['add_expires'] = (int) $_REQUEST['add-expires'];
            } else {
                $this->cache['speed_optimization']['add_expires'] = 0;
            }
            //disabled cache mobile and tablet when other mobile plugin installed
            if(file_exists(WP_PLUGIN_DIR.'/wp-mobile-detect/wp-mobile-detect.php') ||
                file_exists(WP_PLUGIN_DIR.'/wp-mobile-edition/wp-mobile-edition.php') ||
                file_exists(WP_PLUGIN_DIR.'/wptouch/wptouch.php') ||
                file_exists(WP_PLUGIN_DIR.'/wiziapp-create-your-own-native-iphone-app/wiziapp.php') ||
                file_exists(WP_PLUGIN_DIR.'/wordpress-mobile-pack/wordpress-mobile-pack.php')){
                $this->cache['speed_optimization']['devices']['cache_tablet'] = 3;
                $this->cache['speed_optimization']['devices']['cache_mobile'] = 3;
            }
            $opts['speed_optimization'] = $this->cache['speed_optimization'];
            update_option('wpsol_optimization_settings', $opts);
        }
    }

    //save save_settings_advanced_features
    public function save_settings_advanced_features($opts) {
        if (!empty($opts)) {
            if (isset($_REQUEST['html-minification'])) {
                $this->minification['advanced_features']['html_minification'] = (int) $_REQUEST['html-minification'];
            } else {
                $this->minification['advanced_features']['html_minification'] = 0;
            }
            if (isset($_REQUEST['css-minification'])) {
                $this->minification['advanced_features']['css_minification'] = (int) $_REQUEST['css-minification'];
            } else {
                $this->minification['advanced_features']['css_minification'] = 0;
            }
            if (isset($_REQUEST['js-minification'])) {
                $this->minification['advanced_features']['js_minification'] = (int) $_REQUEST['js-minification'];
            } else {
                $this->minification['advanced_features']['js_minification'] = 0;
            }
            if (isset($_REQUEST['cssgroup-minification'])) {
                $this->minification['advanced_features']['cssgroup_minification'] = (int) $_REQUEST['cssgroup-minification'];
            } else {
                $this->minification['advanced_features']['cssgroup_minification'] = 0;
            }
            if (isset($_REQUEST['jsgroup-minification'])) {
                $this->minification['advanced_features']['jsgroup_minification'] = (int) $_REQUEST['jsgroup-minification'];
            } else {
                $this->minification['advanced_features']['jsgroup_minification'] = 0;
            }
            $opts['advanced_features'] = $this->minification['advanced_features'];

            update_option('wpsol_optimization_settings', $opts);
        }
    }
    public static function add_gzip_htacess($check){
        $data = "# Begin Gzipofwpsol".PHP_EOL.
            "<IfModule mod_deflate.c>".PHP_EOL.
            "AddType x-font/woff .woff".PHP_EOL.
            "AddOutputFilterByType DEFLATE image/svg+xml".PHP_EOL.
            "AddOutputFilterByType DEFLATE text/plain".PHP_EOL.
            "AddOutputFilterByType DEFLATE text/html".PHP_EOL.
            "AddOutputFilterByType DEFLATE text/xml".PHP_EOL.
            "AddOutputFilterByType DEFLATE text/css".PHP_EOL.
            "AddOutputFilterByType DEFLATE text/javascript".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/xml".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/xhtml+xml".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/rss+xml".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/javascript".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/x-javascript".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/x-font-ttf".PHP_EOL.
            "AddOutputFilterByType DEFLATE application/vnd.ms-fontobject".PHP_EOL.
            "AddOutputFilterByType DEFLATE font/opentype font/ttf font/eot font/otf".PHP_EOL.
            "</IfModule>".PHP_EOL.
            "# End Gzipofwpsol" . PHP_EOL ;
        if ($check) {
            if (!is_super_admin()) {
                return FALSE;
            }
            //open htaccess file
            $htaccessContent = file_get_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess');
            if (empty($htaccessContent)) {
                return FALSE;
            }
            //if isset Gzip access
            if (strpos($htaccessContent, 'mod_deflate') !== false || strpos($htaccessContent, 'AddOutputFilterByType') !== false || strpos($htaccessContent, 'AddType') !== false || strpos($htaccessContent,'Gzipofwpsol') !== false) {
                return FALSE;
            }

            $htaccessContent = $data.$htaccessContent;
            file_put_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess', $htaccessContent);
            return TRUE;

        } else {
            if (!is_super_admin()) {
                return FALSE;
            }
            //open htaccess file
            $htaccessContent = file_get_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess');
            if (empty($htaccessContent)) {
                return FALSE;
            }

            $htaccessContent = str_replace($data,'',$htaccessContent);
            file_put_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess', $htaccessContent);
            return TRUE;
        }
    }
    public static function add_expires_header($check) {
        $expires = "#Expires headers configuration added by Speed of Light plugin" . PHP_EOL .
                    "<IfModule mod_expires.c>" . PHP_EOL .
                    "   ExpiresActive On" . PHP_EOL .
                    "   ExpiresDefault A2592000" . PHP_EOL .
                    "   ExpiresByType application/javascript \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType text/javascript \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType text/css \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/jpeg \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/png \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/gif \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/ico \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/x-icon \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/svg+xml \"access plus 7 days\"" . PHP_EOL .
                    "   ExpiresByType image/bmp \"access plus 7 days\"" . PHP_EOL .
                    "</IfModule>" . PHP_EOL . 
                    "#End of expires headers configuration" . PHP_EOL ;

        if ($check) {
            if (!is_super_admin()) {
               return FALSE;
            }
            //open htaccess file
            $htaccessContent = file_get_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess');
            if (empty($htaccessContent)) {
                return FALSE;
            }
            //if isset expires header in htacces
            if (strpos($htaccessContent, 'mod_expires') !== false || strpos($htaccessContent, 'ExpiresActive') !== false || strpos($htaccessContent, 'ExpiresDefault') !== false || strpos($htaccessContent, 'ExpiresByType') !== false) {
                return FALSE;
            }

            $htaccessContent = $expires.$htaccessContent;
            file_put_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess', $htaccessContent);
            return TRUE;
             
        } else {
             if (!is_super_admin()) {
               return FALSE;
            }
            //open htaccess file
            $htaccessContent = file_get_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess');
            if (empty($htaccessContent)) {
                return FALSE;
            }
            
            $htaccessContent = str_replace($expires,'',$htaccessContent);
            file_put_contents(ABSPATH . DIRECTORY_SEPARATOR . '.htaccess', $htaccessContent);
            return TRUE;
        }
    }

    /**
     * Return an instance of the current class, create one if it doesn't exist
     * @since  1.0
     * @return object
     */
    public static function factory() {

        static $instance;

        if (!$instance) {
            $instance = new self();
            $instance->set_action();
        }

        return $instance;
    }

}
