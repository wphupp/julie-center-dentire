<?php
if ( ! defined( 'ABSPATH' ) ) exit;
require_once(ABSPATH.'wp-includes/pluggable.php');

class WpSoL_Configuration {

    public function __construct() {
        add_action('load-wp-speed-of-light_page_wpsol_parameters', array($this, 'save_settings'));
        add_action('init',array($this,'loadScriptsAll'));
        add_action('wp_head', array($this,'define_ajaxurl'));
        $opts = get_option('wpsol_configuration');
        if (!empty($opts) && $opts['display_clean'] == 1) {
            add_action('admin_bar_menu', array($this, 'action_admin_bar_menu'), 999);
        }
        add_action('init', array($this, 'disable_optimization_by_role'));
    }
    public function loadScriptsAll(){
        if(current_user_can('manage_options')){
            wp_register_style('style-light-speed', plugins_url('css/style.css', dirname(__FILE__)), array(), WPSOL_VERSION);
            wp_enqueue_style('style-light-speed');
            wp_enqueue_script(
                'wpsol-scripts-speed', plugins_url('js/wpsol-scripts.js', dirname(__FILE__)), array('jquery'), WPSOL_VERSION, true
            );
        }
    }
    //define ajaxurl
    function define_ajaxurl() {
        if(current_user_can('manage_options')){
            echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
             </script>';
        }
    }

    //save settings
    public function save_settings() {
        if(current_user_can('manage_options')){
            WP_Filesystem();
            $opts = get_option('wpsol_configuration');
            if (!empty($_REQUEST['action']) && 'wpsol_configuration' === $_REQUEST['action']) {

                check_admin_referer('wpsol_save_parameter','_wpsol_nonce');

                if (isset($_POST['disable_user'])) {
                    $opts['disable_user'] = 1;
                } else {
                    $opts['disable_user'] = 0;
                }
                if (isset($_POST['display_clean'])) {
                    $opts['display_clean'] = 1;
                } else {
                    $opts['display_clean'] = 0;
                }
                if (isset($_POST['disable_page'])) {

                    $input = $_POST['disable_page'];
                    //decode url when insert russian character to input text
                    $input = rawurldecode($input);
                    $input = trim($input);
                    $input = str_replace(' ', '', $input);
                    $input = explode("\n", $input);

                    $opts['disable_page'] = $input;
                } else {
                    $opts['disable_page'] = array();
                }
                if (isset($_POST['webtest_api_key'])) {
                    $opts['webtest_api_key'] = $_POST['webtest_api_key'];
                }
                update_option('wpsol_configuration', $opts);

                $this->disable_optimization_by_role();
                //write config for cache
                WpSoL_Cache::factory()->write_config_cache();
                //clear cache after save settings
                WpSoL_Cache::wpsol_cache_flush();
                WpSoL_MinificationCache::clear_minification();

                if (!empty($_REQUEST['_wp_http_referer'])) {
                    wp_redirect($_REQUEST['_wp_http_referer'] . '&save-settings=success');
                    exit;
                }
            }
        }


    }

    //create menu bar
    public function action_admin_bar_menu(WP_Admin_Bar $wp_admin_bar) {
        if(current_user_can('manage_options')){
            $title = __('Clean Cache', 'wp-speed-of-light');

            $wp_admin_bar->add_menu(array(
                'id' => 'wpsol-clean-cache-topbar',
                'title' => '<span class="ab-icon"></span><span class="ab-label">' . esc_html($title) . '</span>',
                'href' => '#',
                'meta' => array(
                    'classname' => 'wpsol-cache',
                ),
            ));
        }
    }

    //ajax clean cache
    public static function ajax_clean_cache() {
        $size_cache = 0;
        $size_css_cache = 0;
        $size_js_cache = 0;
        $config = get_option('wpsol_optimization_settings');
        // analysis size cache
        $cachepath = rtrim(WP_CONTENT_DIR, '/') . '/cache/wpsol-cache';
        if (is_dir($cachepath))
            $cachedirs = scandir($cachepath);
        if (!empty($cachedirs)) {
            foreach ($cachedirs as $cachedir) {
                if ($cachedir != '.' && $cachedir != '..') {
                    $filepath = $cachepath . '/' . $cachedir;
                    if(is_dir($filepath))
                    $filedirs = scandir($filepath);
                    foreach($filedirs as $filedir){
                        if ($filedir != '.' && $filedir != '..') {
                            if (@file_exists($filepath)) {
                                $dir_path = $filepath.'/'.$filedir;
                                $size_cache += filesize($dir_path);
                            }
                        }
                    }

                }
            }
        }
        // analysis size css cache
        if(is_multisite()){
            $blog_id = get_current_blog_id();
            $css_path = rtrim(WP_CONTENT_DIR, '/') . '/cache/wpsol-minification/'.$blog_id.'/css';
        }else{
            $css_path = rtrim(WP_CONTENT_DIR, '/') . '/cache/wpsol-minification/css';
        }
        if (is_dir($css_path))
            $file_in_css = scandir($css_path);
        if (!empty($file_in_css)) {
            foreach ($file_in_css as $v) {
                if ($v != '.' && $v != '..' && $v != 'index.html') {
                    $path = $css_path . '/' . $v;
                    $size_css_cache += filesize($path);
                }
            }
        }

        // analysis size js cache
        if(is_multisite()){
            $blog_id = get_current_blog_id();
            $js_path = rtrim(WP_CONTENT_DIR, '/') . '/cache/wpsol-minification/'.$blog_id.'/js';
        }else{
            $js_path = rtrim(WP_CONTENT_DIR, '/') . '/cache/wpsol-minification/js';
        }
        if (is_dir($js_path))
            ;
        $file_in_js = scandir($js_path);
        if (!empty($file_in_js)) {
            foreach ($file_in_js as $v) {
                if ($v != '.' && $v != '..' && $v != 'index.html') {
                    $path = $js_path . '/' . $v;
                    $size_js_cache += filesize($path);
                }
            }
        }

        $total_size_cache = $size_cache + $size_css_cache + $size_js_cache;

        $result = self::formatBytes($total_size_cache);
        //clear minification

        WpSoL_MinificationCache::clear_minification();
        //delete all cache
        WpSoL_Cache::factory()->wpsol_cache_flush();

        echo json_encode($result);
        exit;
    }

    public function disable_optimization_by_role() {
        if(current_user_can('manage_options')){
            $opts = get_option('wpsol_configuration');
            $check = array('disable' => 0);
            if (!empty($opts) && $opts['disable_user'] == 1) {
                if (current_user_can('manage_options')) {
                    $check['disable'] = 1;
                    update_option('wpsol_disable_optimizition_by_role', $check);
                } else {
                    update_option('wpsol_disable_optimizition_by_role', $check);
                }
            } else {
                update_option('wpsol_disable_optimizition_by_role', $check);
            }
            return true;
        }

    }

    public static function wpsol_get_url_path() {
        $total_path = array();
        $total_path[] = get_site_url();
        //get post url
        $arg1 = array(
            'posts_per_page' => -1,
            'post_type' => 'post',
        );
        $posts = get_posts($arg1);
        foreach ($posts as $post) {
            $total_path[] = get_permalink($post->ID);
        }
        //get page url
        $arg2 = array(
            'posts_per_page' => -1,
            'post_type' => 'page',
        );
        $pages = get_posts($arg2);
        foreach ($pages as $page) {
            $total_path[] = get_page_link($page->ID);
        }
        return $total_path;
    }

    public static function formatBytes($bytes, $precision = 2) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2);
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2);
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2);
        } elseif ($bytes > 1) {
            $bytes = $bytes;
        } elseif ($bytes == 1) {
            $bytes = $bytes;
        } else {
            $bytes = '0';
        }

        return $bytes;
    }

}
