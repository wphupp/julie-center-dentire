<?php
if ( ! defined( 'ABSPATH' ) ) exit;
class WpSoL_DatabaseCleanup {
    
    //use query to clean system
    public static function cleanSystem($type){
        global $wpdb;

        check_admin_referer('wpsol_database_clean','_wpsol_nonce');

            $clean = "";

            switch ($type){
                case "revisions":
                    $clean = "DELETE FROM `$wpdb->posts` WHERE post_type = 'revision';";
                    $revisions = $wpdb->query( $clean );

                    $message = "All post revisions";
                    break;
                case "drafted":
                    $clean = "DELETE FROM `$wpdb->posts` WHERE post_status = 'auto-draft';";
                    $autodraft = $wpdb->query( $clean );

                    $message = "All auto drafted content";
                    break;
                case "trash":
                    $clean = "DELETE FROM `$wpdb->posts` WHERE post_status = 'trash';";
                    $posttrash = $wpdb->query( $clean );

                    $message = "All trashed content";
                    break;
                case "comments":
                    $clean = "DELETE FROM `$wpdb->comments` WHERE comment_approved = 'spam' OR comment_approved = 'trash' ;";
                    $comments = $wpdb->query( $clean );

                    $message = "Comments from trash & spam";
                    break;
                case "trackbacks":
                    $clean = "DELETE FROM `$wpdb->comments` WHERE comment_type = 'trackback' OR comment_type = 'pingback' ;";
                    $comments = $wpdb->query( $clean );

                    $message = "Trackbacks and pingbacks";
                    break;
                case "transient":
                    $clean = "DELETE FROM `$wpdb->options` WHERE option_name LIKE '%\_transient\_%' ;";
                    $comments = $wpdb->query( $clean );

                    $message = "Transient options";
                    break;
            }
            $message = "Database cleanup successful";
            return $message;

    }
    
    public function getElementToClean($type){
        global $wpdb;
        $return = 0;
        switch ($type){
            case "revisions":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_type = 'revision';";
                $return = $wpdb->query( $element );
                break;
            case "drafted":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_status = 'auto-draft';";
                $return = $wpdb->query( $element );
                break;
            case "trash":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_status = 'trash';";
                $return = $wpdb->query( $element );
                break;
            case "comments":
                $element = "SELECT * FROM `$wpdb->comments` WHERE comment_approved = 'spam' OR comment_approved = 'trash' ;";
                $return = $wpdb->query( $element );
                break;
            case "trackbacks":
                $element = "SELECT * FROM `$wpdb->comments` WHERE comment_type = 'trackback' OR comment_type = 'pingback' ;";
                $return = $wpdb->query( $element );
                break;
            case "transient":
                $element = "SELECT * FROM `$wpdb->options` WHERE option_name LIKE '%\_transient\_%' ;";
                $return = $wpdb->query( $element );
                break;
        }
        return $return;
    }
}