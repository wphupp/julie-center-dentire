<?php
/* 
 *  Based on some work of query-monitor plugin 
 */
if (!defined('SAVEQUERIES')) {
    define('SAVEQUERIES', true);
}
if (!defined('SOL_DB_EXPENSIVE')) {
    define('SOL_DB_EXPENSIVE', 0.05);
}
if ( ! defined( 'ABSPATH' ) ) exit;
class WpSoL_DB_Queries {

    protected $data = array(
        'types' => array(),
        'component_times' => array(),
    );
    protected $filtered_trace = null;
    protected static $abspath = null;
    protected static $contentpath = null;
    protected static $file_dirs = array();
    protected static $file_components = array();
    protected static $ignore_class = array(
        'wpdb' => true,
        'QueryMonitor' => true,
        'ExtQuery' => true,
        'W3_Db' => true,
        'Debug_Bar_PHP' => true,
    );
    protected static $ignore_method = array();
    protected static $ignore_func = array(
        'include_once' => true,
        'require_once' => true,
        'include' => true,
        'require' => true,
        'call_user_func_array' => true,
        'call_user_func' => true,
        'trigger_error' => true,
        '_doing_it_wrong' => true,
        '_deprecated_argument' => true,
        '_deprecated_file' => true,
        '_deprecated_function' => true,
        'dbDelta' => true,
    );
    protected static $show_args = array(
        'do_action' => 1,
        'apply_filters' => 1,
        'do_action_ref_array' => 1,
        'apply_filters_ref_array' => 1,
        'get_template_part' => 2,
        'get_extended_template_part' => 2,
        'load_template' => 'dir',
        'dynamic_sidebar' => 1,
        'get_header' => 1,
        'get_sidebar' => 1,
        'get_footer' => 1,
        'get_site_by_path' => 3,
    );
    protected static $filtered = false;
    protected $trace = null;
    protected $calling_line = 0;
    protected $calling_file = '';
    public $id = 'db_queries';
    public $db_objects = array();

    public function __construct() {
        $this->trace = debug_backtrace(false);

         if (!defined('DOING_AJAX')) {             
               // Hook shutdown
            register_shutdown_function( array( $this, 'shutdown_handler' ) );          
        }
    }
    
    public function shutdown_handler(){
          $this->process();
    }
    
    protected function log_type($type) {

        if (isset($this->data['types'][$type])) {
            $this->data['types'][$type] ++;
        } else {
            $this->data['types'][$type] = 1;
        }
    }

    protected function maybe_log_dupe($sql, $i) {

        $sql = str_replace(array("\r\n", "\r", "\n"), ' ', $sql);
        $sql = str_replace(array("\t", '`'), '', $sql);
        $sql = preg_replace('/[ ]+/', ' ', $sql);
        $sql = trim($sql);

        $this->data['dupes'][$sql][] = $i;
    }

    protected function log_component($component, $ltime, $type) {
        
        if (!isset($this->data['component_times'][$component->name])) {
            $this->data['component_times'][$component->name] = array(
                'component' => $component->name,
                'calls' => 0,
                'ltime' => 0,
                'types' => array()
            );
        }

        $this->data['component_times'][$component->name]['calls'] ++;
        $this->data['component_times'][$component->name]['ltime'] += $ltime;

        if (isset($this->data['component_times'][$component->name]['types'][$type])) {
            $this->data['component_times'][$component->name]['types'][$type] ++;
        } else {
            $this->data['component_times'][$component->name]['types'][$type] = 1;
        }
    }

    public function get_errors() {
        if (!empty($this->data['errors'])) {
            return $this->data['errors'];
        }
        return false;
    }

    public static function is_expensive(array $row) {
        return $row['ltime'] > SOL_DB_EXPENSIVE;
    }

    public function process() {
        if (!SAVEQUERIES) {
            return;
        }
        $this->data['total_qs'] = 0;
        $this->data['total_time'] = 0;
        $this->data['errors'] = array();
        $this->db_objects = apply_filters('db_objects', array(
            '$wpdb' => $GLOBALS['wpdb']
                ));
        foreach ($this->db_objects as $name => $db) {
            
            if (is_a($db, 'wpdb')) {
                $this->process_db_object($name, $db);
            } else {
                unset($this->db_objects[$name]);
            }
        }
    }

    protected function log_caller($caller, $ltime, $type) {

        if (!isset($this->data['times'][$caller])) {
            $this->data['times'][$caller] = array(
                'caller' => $caller,
                'calls' => 0,
                'ltime' => 0,
                'types' => array()
            );
        }

        $this->data['times'][$caller]['calls'] ++;
        $this->data['times'][$caller]['ltime'] += $ltime;

        if (isset($this->data['times'][$caller]['types'][$type])) {
            $this->data['times'][$caller]['types'][$type] ++;
        } else {
            $this->data['times'][$caller]['types'][$type] = 1;
        }
    }

    public function process_db_object($id, wpdb $db) {
        global $EZSQL_ERROR;

        $rows = array();
        $types = array();
        $total_time = 0;
        $has_result = false;
        $has_trace = false;
        $i = 0;

        foreach ((array) $db->queries as $query) {
          
            # @TODO: decide what I want to do with this:
            if (false !== strpos($query[2], 'wp_admin_bar') and ! isset($_REQUEST['qm_display_admin_bar'])) {
                continue;
            }
            
            $sql = $query[0];
            $ltime = $query[1];
            $stack = $query[2];
            $has_trace = isset($query['trace']);
            $has_result = isset($query['result']);

            if (isset($query['result'])) {
                $result = $query['result'];
            } else {
                $result = null;
            }
            
            $total_time += $ltime;
            $component = $this->get_component();

            if (isset($query['trace'])) {
                $trace = $query['trace'];
                $caller = $this->get_caller();
                $caller_name = $caller['id'];
                $caller = $caller['display'];
            } else {

                $trace = null;
                $component = null;
                $callers = explode(',', $stack);
                $caller = trim(end($callers));

                if (false !== strpos($caller, '(')) {
                    $caller_name = substr($caller, 0, strpos($caller, '(')) . '()';
                } else {
                    $caller_name = $caller;
                }
            }

            $sql = trim($sql);
            $type = $this->get_query_type($sql);

            $this->log_type($type);
            $this->log_caller($caller_name, $ltime, $type);

            $this->maybe_log_dupe($sql, $i);

            if ($component) {
                $this->log_component($component, $ltime, $type);
            }

            if (!isset($types[$type]['total'])) {
                $types[$type]['total'] = 1;
            } else {
                $types[$type]['total'] ++;
            }

            if (!isset($types[$type]['callers'][$caller])) {
                $types[$type]['callers'][$caller] = 1;
            } else {
                $types[$type]['callers'][$caller] ++;
            }

            $row = compact('caller', 'caller_name', 'stack', 'sql', 'ltime', 'result', 'type', 'component', 'trace');

            if (is_wp_error($result)) {
                $this->data['errors'][] = $row;
            }

            if (self::is_expensive($row)) {
                $this->data['expensive'][] = $row;
            }

            $rows[$i] = $row;
            $i++;
        }

        if ('$wpdb' === $id && !$has_result && !empty($EZSQL_ERROR) && is_array($EZSQL_ERROR)) {
            // Fallback for displaying database errors when wp-content/db.php isn't in place
            foreach ($EZSQL_ERROR as $error) {
                $row = array(
                    'caller' => 'Unknown',
                    'caller_name' => 'Unknown',
                    'stack' => array(),
                    'sql' => $error['query'],
                    'result' => new WP_Error('qmdb', $error['error_str']),
                    'type' => '',
                    'component' => false,
                    'trace' => null,
                );
                $this->data['errors'][] = $row;
            }
        }

        $total_qs = count($rows);

        $this->data['total_qs'] += $total_qs;
        $this->data['total_time'] += $total_time;

        # @TODO put errors in here too:
        # @TODO proper class instead of (object)
        $this->data['dbs'][$id] = (object) compact('rows', 'types', 'has_result', 'has_trace', 'total_time', 'total_qs');
        $queries = get_option('wpsol_scan_queries');
        if (!empty($queries)) {
            $queries = array();
        }
        $queries = $this->data;
        update_option('wpsol_scan_queries', $queries);
    }

    public static function get_query_type($sql) {
        $sql = $type = trim($sql);

        if (0 === strpos($sql, '/*')) {
            // Strip out leading comments such as `/*NO_SELECT_FOUND_ROWS*/` before calculating the query type
            $type = preg_replace('|^/\*[^\*/]+\*/|', '', $sql);
        }

        $type = preg_split('/\b/', trim($type), 2, PREG_SPLIT_NO_EMPTY);
        $type = strtoupper($type[0]);

        return $type;
    }

    public function get_component() {

        $components = array();

        foreach ($this->trace as $item) {

            try {

                if (isset($item['class'])) {
                    if (!is_object($item['class']) and ! class_exists($item['class'], false)) {
                        continue;
                    }
                    if (!method_exists($item['class'], $item['function'])) {
                        continue;
                    }
                    $ref = new ReflectionMethod($item['class'], $item['function']);
                    $file = $ref->getFileName();
                } else if (function_exists($item['function'])) {
                    $ref = new ReflectionFunction($item['function']);
                    $file = $ref->getFileName();
                } else if (isset($item['file'])) {
                    $file = $item['file'];
                } else {
                    continue;
                }
               
                $comp = $this->get_file_component($file);
                $components[$comp->type] = $comp;
            } catch (ReflectionException $e) {
                # nothing
            }
        }

        foreach ($this->get_file_dirs() as $type => $dir) {
            if (isset($components[$type])) {
                return $components[$type];
            }
        }

        # This should not happen
    }

    public function get_caller() {

        $trace = $this->get_filtered_trace();
        return reset($trace);
    }

    public function get_filtered_trace() {

        if (!isset($this->filtered_trace)) {
            $trace = array_map(array($this, 'filter_trace'), $this->trace);
            $trace = array_values(array_filter($trace));
            if (empty($trace) && !empty($this->trace)) {
                $lowest = $this->trace[0];
                $file = self::standard_dir($lowest['file'], '');
                $lowest['calling_file'] = $lowest['file'];
                $lowest['calling_line'] = $lowest['line'];
                $lowest['function'] = $file;
                $lowest['display'] = $file;
                $lowest['id'] = $file;
                unset($lowest['class'], $lowest['args'], $lowest['type']);
                $trace[0] = $lowest;
            }

            $this->filtered_trace = $trace;
        }

        return $this->filtered_trace;
    }

    public static function get_file_component($file) {
        
        # @TODO turn this into a class (eg SOL_File_Component)

        $file = self::standard_dir($file);

        if (isset(self::$file_components[$file])) {
            return self::$file_components[$file];
        }

        foreach (self::get_file_dirs() as $type => $dir) {
            if ($dir && ( 0 === strpos($file, $dir) )) {
                break;
            }
        }

        $context = $type;

        switch ($type) {
            case 'plugin':
            case 'mu-plugin':
                $plug = plugin_basename($file);
                if (strpos($plug, '/')) {
                    $plug = explode('/', $plug);
                    $plug = reset($plug);
                } else {
                    $plug = basename($plug);
                }
                if ('mu-plugin' === $type) {
                    $name = sprintf(__('MU Plugin: %s', 'wp-speed-of-light'), $plug);
                } else {
                    $name = sprintf(__('Plugin: %s', 'wp-speed-of-light'), $plug);
                }
                $context = $plug;
                break;
            case 'go-plugin':
            case 'vip-plugin':
                $plug = str_replace(self::$file_dirs[$type], '', $file);
                $plug = trim($plug, '/');
                if (strpos($plug, '/')) {
                    $plug = explode('/', $plug);
                    $plug = reset($plug);
                } else {
                    $plug = basename($plug);
                }
                $name = sprintf(__('VIP Plugin: %s', 'wp-speed-of-light'), $plug);
                $context = $plug;
                break;
            case 'stylesheet':
                if (is_child_theme()) {
                    $name = __('Child Theme', 'wp-speed-of-light');
                } else {
                    $name = __('Theme', 'wp-speed-of-light');
                }
                break;
            case 'template':
                $name = __('Parent Theme', 'wp-speed-of-light');
                break;
            case 'other':
                $name = self::standard_dir($file, '');
                $context = $file;
                break;
            case 'core':
                $name = __('Core', 'wp-speed-of-light');
                break;
            case 'unknown':
            default:
                $name = __('Unknown', 'wp-speed-of-light');
                break;
        }

        return self::$file_components[$file] = (object) compact('type', 'name', 'context');
    }

    public static function standard_dir($dir, $path_replace = null) {

        $dir = wp_normalize_path($dir);

        if (is_string($path_replace)) {
            if (!self::$abspath) {
                self::$abspath = wp_normalize_path(ABSPATH);
                self::$contentpath = wp_normalize_path(dirname(WP_CONTENT_DIR) . '/');
            }
            $dir = str_replace(array(
                self::$abspath,
                self::$contentpath,
                    ), $path_replace, $dir);
        }

        return $dir;
    }

    public static function get_file_dirs() {
        if (empty(self::$file_dirs)) {
            self::$file_dirs['plugin'] = self::standard_dir(WP_PLUGIN_DIR);
            self::$file_dirs['go-plugin'] = self::standard_dir(WPMU_PLUGIN_DIR . '/shared-plugins');
            self::$file_dirs['mu-plugin'] = self::standard_dir(WPMU_PLUGIN_DIR);
            self::$file_dirs['vip-plugin'] = self::standard_dir(get_theme_root() . '/vip/plugins');
            self::$file_dirs['stylesheet'] = self::standard_dir(get_stylesheet_directory());
            self::$file_dirs['template'] = self::standard_dir(get_template_directory());
            self::$file_dirs['other'] = self::standard_dir(WP_CONTENT_DIR);
            self::$file_dirs['core'] = self::standard_dir(ABSPATH);
            self::$file_dirs['unknown'] = null;
        }
        return self::$file_dirs;
    }

    public function filter_trace(array $trace) {

        if (!self::$filtered and function_exists('did_action') and did_action('plugins_loaded')) {

            # Only run apply_filters on these once
            self::$ignore_class = apply_filters('ignore_class', self::$ignore_class);
            self::$ignore_method = apply_filters('ignore_method', self::$ignore_method);
            self::$ignore_func = apply_filters('ignore_func', self::$ignore_func);
            self::$show_args = apply_filters('show_args', self::$show_args);
            self::$filtered = true;
        }

        $return = $trace;

        if (isset($trace['class'])) {

            if (isset(self::$ignore_class[$trace['class']])) {
                $return = null;
            } else if (isset(self::$ignore_method[$trace['class']][$trace['function']])) {
                $return = null;
            } else if (0 === strpos($trace['class'], 'SOL_')) {
                $return = null;
            } else {
                $return['id'] = $trace['class'] . $trace['type'] . $trace['function'] . '()';
                $return['display'] = $trace['class'] . $trace['type'] . $trace['function'] . '()';
            }
        } else {

            if (isset(self::$ignore_func[$trace['function']])) {

                $return = null;
            } else if (isset(self::$show_args[$trace['function']])) {

                $show = self::$show_args[$trace['function']];

                if ('dir' === $show) {
                    if (isset($trace['args'][0])) {
                        $arg = self::standard_dir($trace['args'][0], '~/');
                        $return['id'] = $trace['function'] . '()';
                        $return['display'] = $trace['function'] . "('{$arg}')";
                    }
                } else {
                    $args = array();
                    for ($i = 0; $i < $show; $i++) {
                        if (isset($trace['args'][$i])) {
                            $args[] = '\'' . $trace['args'][$i] . '\'';
                        }
                    }
                    $return['id'] = $trace['function'] . '()';
                    $return['display'] = $trace['function'] . '(' . implode(',', $args) . ')';
                }
            } else {

                $return['id'] = $trace['function'] . '()';
                $return['display'] = $trace['function'] . '()';
            }
        }

        if ($return) {

            $return['calling_file'] = $this->calling_file;
            $return['calling_line'] = $this->calling_line;
        }

        if (isset($trace['line'])) {
            $this->calling_line = $trace['line'];
        }
        if (isset($trace['file'])) {
            $this->calling_file = $trace['file'];
        }

        return $return;
    }

}
