<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>
<section class="error-404 not-found">
	<div class="row">
		<div class="container">
			<div class="centered_404">
				<img src="<?php the_field('404_image', 'option'); ?>">
					<h1>Fichier non trouvé</h1>
					<p>Désolé, ce fichier ne vit plus ici. Il aurait pu être déplacé ou rendu privé.</p>
					<a href="http://localhost/julie-center-dentire/">Retour à l'accueil</a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>