<?php get_header(); ?>
<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<?php if($thumb['0']) { ?>
<section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>">
</section>
<?php } ?>
<section>
	<div class="row">
		<div class="container">
			<article id="post-<?php the_ID(); ?>" class="columns small-12 medium-9">
				<h1><?php the_title();?></h1>
				<?php the_content(); ?>
			</article>
			<aside class="columns small-12 medium-5 large-3 sidebar">
				<?php if(is_active_sidebar('sidebar1')){ dynamic_sidebar('sidebar1'); } ?>
			</aside>
		</div>
	</div>
</section>

<?php get_footer();?>