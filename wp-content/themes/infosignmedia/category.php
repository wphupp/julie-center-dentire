<?php
/**
* The template for displaying category pages
*/

get_header(); ?>

<?php if($thumb = wp_get_attachment_image_src( get_post_thumbnail_id('29'), 'full' )) :?>
  <section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>"></section>
<?php endif; ?>

<section>
  <div class="row container">
    <?php if ( have_posts() ) : ?>

      <?php
      the_archive_title( '<h1>', '</h1>' );
      the_archive_description( '<div class="taxonomy-description">', '</div>' );
      ?>
      <?php
      while ( have_posts() ) : the_post();?>
      <article class="information" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
        <?php the_excerpt();?>
      </article>
      <?php
      endwhile;
      the_posts_pagination( array() );
      else :
        get_template_part( 'content', 'none' );
      endif;
      ?>
    </div>
  </section>

  <?php get_footer(); ?>