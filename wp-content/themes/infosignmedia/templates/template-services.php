<?php
/**
 * Template Name: Services
 *
*/
get_header();
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>

  
<?php the_content(); ?>
<section class="services">
	  <div class="row">
			<div class="container">
						<div class="columns small-12 medium-12 large-12 servie-main">
						   <div class="columns small-12 medium-6 large-6 servie-left" style=" background: url('<?php the_field('background_image'); ?>'); ">
									<div class="main-serive-page">
													<div class="service-content">
														<h1><?php the_field('service_title'); ?></h1>
														<p><?php the_field('service_description'); ?></p>
													</div>
									</div>
							</div>
					
				<div class="columns small-12 medium-6 large-6 servie-rigth">
						<div class="listing-page">	
						   
							<?php
								if($config['services_category']){
									
									// Classé par catégories
									
									$args = array( 
										'post_type' => 'services',
										'posts_per_page' => -1, 
										'post_parent' => 0, 
										'orderby'  => 'menu_order',
										'order' => 'ASC'
									);
									$services = new WP_Query( $args );
									
									while ( $services->have_posts() ) : $services->the_post(); ?>
										<article class="listing-service" id="post-<?php echo $post->ID; ?>" <?php post_class(); ?>>
										 
										 
										 
										 <div class="step-icon">
												<a href="<?php the_permalink(); ?>">
												<?php 
												
												if ( get_field( 'service_image' ) ){
												$aicon = get_field( 'service_image' );
												echo $aicon = file_get_contents($aicon);
												 }
												
												?>
												
												</a>
											</div>
										 <a href="<?php echo get_permalink($post->ID); ?>"> 
											<h2 class="category"><?php echo the_title(); ?></h2></a>
											<ul class="row">
												<?php
													$args = array( 
														'post_type' => 'services',
														'posts_per_page' => -1,
														'post_parent' => $post->ID, 
														'orderby'  => 'title',
														'order' => 'ASC'
													);
													$service_children = get_posts( $args );
													foreach ( $service_children as $child ) { ?>
														<li class="columns small-12 medium-4"><a href="<?php echo get_permalink($child->ID); ?>"><h3><?php echo get_the_title($child); ?></h3></a></li>
													<?php } ?>
											</ul>
										</article>
									<?php endwhile; 
								} else {
									
									// Sans catégories
									
									$args = array( 
										'post_type' => 'services',
										'posts_per_page' => -1, 
										'post_parent' => 0, 
										'orderby'  => 'menu_order'
									);
									$services_parent = new WP_Query( $args );
									while ( $services_parent->have_posts() ) : $services_parent->the_post(); 
										$parents[] = $post->ID;
									endwhile;
									wp_reset_query();
									
									$args = array( 
										'post_type' => 'services',
										'posts_per_page' => -1, 
										'orderby'  => 'title',
										'post__not_in' => $parents,
										'order' => 'ASC'
									);
									$services = new WP_Query( $args ); ?>
							
									<ul class="row">
										<?php while ( $services->have_posts() ) : $services->the_post(); ?>
											<li class="columns small-12 medium-4"><a href="<?php echo get_permalink($post->ID); ?>"><h3><?php echo the_title(); ?></h3></a></li>
										<?php endwhile; ?>
									</ul>
								<?php } ?>
							<?php wp_reset_query(); ?>
						</div>
				</div>	
						</div>
			</div>	
	  </div>
</section>
<?php get_footer();?>