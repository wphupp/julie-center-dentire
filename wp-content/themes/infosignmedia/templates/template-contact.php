<?php
/**
 * Template Name: Contact
**/
get_header(); ?>

<section>
	  <div class="row">
			<div class="container">
				  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
				  </article>
			</div>
	  </div>
</section>
<?php get_footer();?>