<?php
/**
* Template Name: Equipe
*/
get_header();

?>

<section class="content">
    <div class="row">
        <div class="container">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php the_content(); ?>
            </article>
        </div>
    </div>
</section>

<?php get_footer();?>