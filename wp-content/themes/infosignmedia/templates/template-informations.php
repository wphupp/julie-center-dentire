<?php
/**
 * Template Name: Informations
*/
get_header();
?>
  
<section class="information">
		<div class="row">
			<div class="container">
					<?php
						while ( have_posts() ) : the_post();?>
						<div class="row">
							  <div class="columns small-12 medium-12 large-12 info-patient">
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									  <?php  the_content(); ?>
									  	<div class="columns small-12 medium-6 large-6 info-left">
											<div class="inner-left">
												<h1><?php the_field('information_title'); ?></h1>
													<?php the_field('information_description'); ?>
											</div>
										</div>
									  
											<div class="columns small-12 medium-6 large-6 info-right">
													<div class="info-background">
														<img src="<?php the_field('information_image'); ?>">
													</div>
													<div class="information-widget-section blue-heading gray columns small-12 medium-6 large-6">	
														<?php  dynamic_sidebar( 'les-formulaires' ); ?>
													</div>	
													
													<div class="information-widget-section blue-heading columns small-12 medium-6 large-6">		
														<?php  dynamic_sidebar( 'les-conseils' ); ?>
													</div>
													
													<div class="information-widget-section blue-heading columns small-12 medium-6 large-6">							
														<?php  dynamic_sidebar( 'les-articles' ); ?>
													</div>
													
													<div class="information-widget-section dark-blue columns small-12 medium-6 large-6">		
														<?php  dynamic_sidebar( 'dark-blue' ); ?>
													</div>	
											</div>
									</article>
							  </div>
						</div>
				<?php endwhile;?>
			</div>
		</div>
</section>
<?php get_footer(); ?>