<?php
/**
 * Template Name: Clinique
*/
get_header(); ?>

<section class="content">
	<div class="row">
		<div class="container">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<?php the_content(); ?>
				
				<div class="clinic">
					<?php if( have_rows('clinique_carousel_title') ): ?>
					<?php while( have_rows('clinique_carousel_title') ): the_row(); ?>
					
						<div class="main-overlay">       
							<img class="img-overlay" src="<?php the_sub_field('clinique_image'); ?>"> 
								<div class="overlay">
									<div class="text-overlay">	
										<h1><?php the_sub_field('clinique_title'); ?></h1>
										<p><?php $summary = get_sub_field('clinique_description');
													echo substr($summary, 0, 350); ?></p>
									</div>
								</div>
						</div>	
				
					 <?php endwhile; ?>
					<?php endif; ?>
				</div>	
			</article>
		</div>
	</div>
</section>

<section>
	 <div class="row box">
		   <?php if( have_rows('photo_clinique') ):
		   while ( have_rows('photo_clinique') ) : the_row();?>
		   <a href="<?php the_sub_field('photo'); ?>" data-lightbox="roadtrip">
			 <div class="columns small-12 medium-4 bgImage" style="<?php backgroundImage(get_sub_field('photo')); ?>"></div>
		   </a>
		 <?php endwhile;endif; ?>
	</div>
</section>

<?php get_footer();?>
