<?php 

$config = array(
	'loading'					=> true,
	'wpml' 						=> true,
	'caroussel' 				=> true,
	'services_category' 		=> true,
	'back_to_top_button' 		=> true,
	'responsive_debug'			=> true,
);
 ?>