<?php
/**
* The template for displaying a "No posts found" message */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h1><?php _e( 'Nothing Found', 'twentyfourteen' ); ?></h1>
	<?php if ( is_search() ) : ?>

		<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyfourteen' ); ?></p>
		<?php get_search_form(); ?>

	<?php else : ?>

		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfourteen' ); ?></p>
		<?php get_search_form(); ?>

	<?php endif; ?>
</article>