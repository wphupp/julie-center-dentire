
<?php get_header(); ?>

<!-- if carousel is activate -->
<?php if($config['caroussel']) : ?>
	<div id="carousel" class="owl-carousel">
		<?php if( have_rows('slides') ): ?>
			<?php while ( have_rows('slides') ) : ?> 
				<?php the_row(); ?>
				<div class="item" style="<?php backgroundImage(get_sub_field('image')); ?>">
					<div class="centered">
						<h2><?php the_sub_field('titre'); ?><br/>
							<?php the_sub_field('sous_titre'); ?>
							<?php the_sub_field('description'); ?></h2>
						<?php if(get_sub_field('sous_titre') != ""): ?>
					
						<?php endif; ?>
						<?php if(get_sub_field('description') != ""): ?>
							
						<?php endif; ?>
						<?php if( have_rows('button') ): ?>
							<?php while ( have_rows('button') ) : ?> 
								<?php the_row(); ?>
								<a <?php echo (get_sub_field('lien_externe'))?'target="_blank"':''; ?>href="<?php the_sub_field('url'); ?>"><?php the_sub_field('texte'); ?></a>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php endif; ?>

<section>
	<div class="row">
		<div class="container">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
			</article>
		</div>
	</div>
</section>
<?php get_footer(); ?>