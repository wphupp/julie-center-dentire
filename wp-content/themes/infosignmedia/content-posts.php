<div class="row">
  <div class="columns small-12 medium-9 single_information">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <h1><?php the_title();?></h1>
      <?php the_content(); ?>
    </article>
  </div>
  <aside class="columns small-12 medium-3 sidebar">
    <?php if(is_active_sidebar('sidebar1')){ dynamic_sidebar('sidebar1'); } ?>  
  </aside>
</div>