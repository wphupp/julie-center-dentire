<?php
/**
* The template part for displaying results in search pages
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<?php $thubs_search =(get_the_post_thumbnail())?true:false; ?>
		<?php if($thubs_search) : ?>
			<div class="columns small-12 medium-4">
				<?php the_post_thumbnail(); ?>
			</div>
		<?php endif; ?>
		<div class="columns small-12 medium-<?php echo ($thubs_search)?'8':'10'; ?>">
			<h3>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</h3>
			<?php the_excerpt(); ?>
			<div class="h-readmore"> 
				<a class="btn" href="<?php the_permalink(); ?>"><?php echo _e('En savoir plus', 'infosignmedia'); ?></a>
			</div>
			<hr>
		</div>
	</div>
</article><!-- #post-## -->