<?php
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>
<section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>"></section>
<section class="services services-list">
	<div class="row">
		<div class="container">
			<h1><?php the_title();?></h1>
			<ul class="row">
				<?php
				$args = array(
					'post_type'      => 'services',
					'posts_per_page' => -1,
					'post_parent'    => $post->ID,
					'order'          => 'ASC',
					'orderby'        => 'menu_order'
					);
				$parent = new WP_Query( $args );
				if ( $parent->have_posts() ) : ?>
				<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
					<li class="columns small-12 medium-3">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3><?php the_title(); ?></h3></a>

					</li>
				<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
		</ul>
	</div>
</div>
</section>