<?php global $config; ?>
</section>
<footer>
  <!--widgets footer-->
	 <div class="main-footer">
			<div class="inner-footer">
				 <div class="footer-left">
					  <div class="footer-section-left">
							<img src="<?php the_field('footer_logo', 'option'); ?>">
						</div>
					<div class="logo-content">
						<p><?php the_field('footer_text', 'option'); ?></p>
					</div>
				</div>
			  
				<div class="footer-right">
						<div class="footer-section-right">
								
							<div class="address"> 
								<h3 class="footer-heading"><?php the_field('footer_address_title', 'option'); ?></h3>
								<?php the_field('footer_address', 'option'); ?>
							</div>
						
								<?php if( have_rows('social_media', 'option') ): ?>
								<?php while( have_rows('social_media', 'option') ): the_row(); ?>
											<a class="social-icon" href=" <?php the_sub_field('social_media_link'); ?>" target="_blank">
												<img src="<?php the_sub_field('social_media_image'); ?>">
											</a>
				   
								<?php endwhile; ?>
								<?php endif; ?>
						</div>
					  
						<div class="footer-section-right">
								
							<div class="footer-nous">
								<h3 class="footer-heading"><?php the_field('footer_nous_joindre_title', 'option'); ?></h3>
								<?php the_field('footer_main_phone_number', 'option'); ?>
								<p><?php the_field('footer_secondary_telephone_number', 'option'); ?></p>
									<?php the_field('footer_email', 'option'); ?>
									
							</div>
						</div>
			
						<div class="footer-section-right-one">
								<h3 class="footer-heading"><?php the_field('footer_heures_d’ouvertire_title', 'option'); ?></h3>
								<?php the_field('footer_heures_d’ouverture', 'option'); ?>
						</div>
				</div>
			</div>
	</div>
 
</footer>


<section class="copyright">
    <p> <?php the_field('copy_right', 'option'); ?></p>
</section>

</div> <!--site_content-->
</div><!-- end wrap -->

<?php if($config['back_to_top_button']) : ?>
  <div id="back_top">
    <span class="ti-angle-up"></span>
  </div>
<?php endif; ?>

<?php if($config['responsive_debug']) : ?>
<div class="debug_responsive">
  <span></span>
</div>
<?php endif; ?>

<?php wp_footer(); ?>

<!-- pour la map dynamique avec page option -->
<?php $map = array();
if( have_rows('informations_de_cliniques', 'option') ) {
  while( have_rows('informations_de_cliniques', 'option') ){
    the_row();
    $map[] = array(
      get_sub_field('clinic_name'),
      'undefined',
      get_sub_field('clinic_phone'),
      get_sub_field('clinic_email'),
      'undefined',
      get_sub_field('latitude'),
      get_sub_field('longitude'),
      get_assets().'images/logo-marker-contact.png'
      );
  }
} ?>

<!-- Javascript utility helper variable -->
<script type="text/javascript">
  var mapArray = <?php echo json_encode($map); ?>;
  var mapStyle = <?php echo json_encode(strip_tags(get_field('style_map', 'option'))); ?>;
</script>

<script type="text/javascript" src="<?php echo get_assets().'/js/custom.js'; ?>"></script>


</body>
</html>
