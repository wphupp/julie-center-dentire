<?php 
	get_header(); 
?>

<div class="zone-enfant">

	<div class="columns small-12 medium-6 sky-blue">
			
			<div class="columns small-12 medium-12 single-post-zone">
							<div class="zone-icon-single-page">
								<img src="<?php  the_field('zone_image'); ?>">
							</div>
						<h1 class="post-title"><?php the_title();?></h1>
						<?php the_field('textarea'); ?>
					<div class="single-zone-content">
						<?php   the_content(); ?>
					</div>
			</div>
	</div>

	<div class="columns small-12 medium-6 main-zone">
		<div class="zone-background single-zone" style=" display: grid; background: url('<?php the_field('zone_default_image', 'option'); ?>'); ">
			<div class="zone-listing-page">	
					<?php
					if($config['services_category']){
						
						// Classé par catégories
						
						$args = array( 
							'post_type' => 'zone',
							'posts_per_page' => -1, 
							'post_parent' => 0, 
							'orderby'  => 'menu_order',
							'order' => 'ASC'
						);
						$services = new WP_Query( $args );
						
						while ( $services->have_posts() ) : $services->the_post(); ?>
							<article class="listing-zone" id="post-<?php echo $post->ID; ?>" <?php post_class(); ?>>
								<div class="step-icon-zone">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php  the_field('zone_image'); ?>">
									</a>
								</div>
								<a href="<?php echo get_permalink($post->ID); ?>"> 
								<h2 class="category"><?php echo the_title(); ?></h2></a>
								<ul class="row">
									<?php
										$args = array( 
											'post_type' => 'zone',
											'posts_per_page' => -1,
											'post_parent' => $post->ID, 
											'orderby'  => 'title',
											'order' => 'ASC'
										);
										$service_children = get_posts( $args );
										foreach ( $service_children as $child ) { ?>
											<li class="columns small-12 medium-4"><a href="<?php echo get_permalink($child->ID); ?>"><h3><?php echo get_the_title($child); ?></h3></a></li>
										<?php } ?>
								</ul>
							</article>
							<?php endwhile; 
					} else {
						
						// Sans catégories
						
						$args = array( 
							'post_type' => 'zone',
							'posts_per_page' => -1, 
							'post_parent' => 0, 
							'orderby'  => 'menu_order'
						);
						$services_parent = new WP_Query( $args );
						while ( $services_parent->have_posts() ) : $services_parent->the_post(); 
							$parents[] = $post->ID;
						endwhile;
						wp_reset_query();
						
						$args = array( 
							'post_type' => 'zone',
							'posts_per_page' => -1, 
							'orderby'  => 'title',
							'post__not_in' => $parents,
							'order' => 'ASC'
						);
						$services = new WP_Query( $args ); ?>
				
						<ul class="row">
							<?php while ( $services->have_posts() ) : $services->the_post(); ?>
								<li class="columns small-12 medium-4"><a href="<?php echo get_permalink($post->ID); ?>"><h3><?php echo the_title(); ?></h3></a></li>
							<?php endwhile; ?>
						</ul>
					<?php } ?>
				<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>
</div>       
<?php get_footer(); ?>