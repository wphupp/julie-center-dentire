<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
*/

get_header(); ?>

<section>
	<div class="row">
		<div class="container">
				<div class="columns small-12 medium-12 large-12  info-patient">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="columns small-12 medium-6 large-6 info-left single-information">
								<div class="inner-left">
									<div class="row">
										<?php
										while ( have_posts() ) : the_post();
										get_template_part( 'content', 'posts' );
										endwhile;
										?>
									</div>
									<?php the_field('left_section'); ?>
								</div>
							</div>
						
						<div class="columns small-12 medium-6 large-6 info-right">
								<?php if( get_field('right_image') ): ?>
								<div class="info-background">
										<img src="<?php the_field('right_image'); ?>" />
								</div>
								<?php else: ?>
								<div class="info-background">
									<img src="<?php the_field('single_image_information_page', 'option'); ?>">
								</div>
								<?php endif; ?>
									<div class="gray information-widget-section blue-heading columns small-12 medium-6 large-6">	
										<?php  dynamic_sidebar( 'les-formulaires' ); ?>
									</div>	
									
									<div class="information-widget-section blue-heading columns small-12 medium-6 large-6">		
										<?php  dynamic_sidebar( 'les-conseils' ); ?>
									</div>
									
									<div class="information-widget-section blue-heading columns small-12 medium-6 large-6">							
										<?php  dynamic_sidebar( 'les-articles' ); ?>
									</div>
									
									<div class="information-widget-section dark-blue columns small-12 medium-6 large-6">		
										<?php  dynamic_sidebar( 'dark-blue' ); ?>
									</div>	
						</div>
							
							
						</article>
				</div>
		</div>	
	</div>
</section>

<?php get_footer(); ?>