<?php 
	get_header(); 
?>
<?php 
$code= get_field('code_clip');
?>

<div class="columns small-12 medium-7 single-ser">
	<div class="columns small-12 medium-12 single-post-serive">
        <div class="service-icon-single-page">
					
					<?php 
												
						if ( get_field( 'service_image' ) ){
						$aicon = get_field( 'service_image' );
						echo $aicon = file_get_contents($aicon);
					 }
										
					?>
                           
        </div>
            <h1 class="post-title"><?php the_title();?></h1>
	</div>
		
		<div class="single-content">
            <?php the_field('textarea'); ?>
            <?php   the_content(); ?>
		</div>	
</div>


<div class="columns small-12 medium-5 single-page-service" style="background: url('<?php the_field('default_image', 'option'); ?>'); ">
	<div class="single_services_video_page">
		<div class="inner_services_video_page">	
			<?php
			if( get_field('code_clip') ) {
			 ?>
					 <script type="text/javascript">
						document.write(insertClipVzaar("<?php  echo $code; ?>", rootFr, "595", "344", "false", "s", ""));
					</script>
			<?php
			}
			elseif( get_field('generic_image') )
			{   
			?>
			<img src="<?php the_field('generic_image'); ?>">
					
			<?php
			}
			else
			{
			?>
				<img src="<?php the_field('single_post_image', 'option'); ?>"  />
			<?php
			}
			?>
		</div>	
    </div>  
</div>

<?php get_footer(); ?>