
//global variable
var tl = new TimelineMax();

//on ready
$(function(){

 FastClick.attach(document.body);

//detect devise and addd class desktop, tablet or mobile in body
detectDevise();

//function that change the behavior of anchor like backbone or angular
preloader();

//set content and main height
setContentAndMainHeight();

//for responsive debug
responsive_debug();
});


//function that change the behavior of anchor like backbone or angular
function preloader() {
//if loading is activate
if($("#overlay").css('display')) {
	tl.to($("#overlay"),0.6, {x: '-100%', onComplete:pageLoaded, ease: Expo.easeInOut}, 1);

	function pageLoaded(){
		$('body').css('overflow', 'auto');
	}

//click event on all a tag to smooth transition refresh page
$(':root').on('click', 'a', function(e){
	if($(this).hasClass('link') || $(this)[0].hasAttribute("target") || $(this).attr('href') == "#"){
		return true;
	}else{
		$("#overlay").css('display', 'block');
		tl.to($("#overlay"),0.6, {x: '0%', ease: Expo.easeInOut, onComplete:refresh, onCompleteParams:[$(this)]});
		return false;
	}
});

		//refresh page
		function refresh(elem){
			window.location = elem.attr("href");
		}
	}
}

//======== SEARCH ===============
$('.search_icon').on('click', function(e){
	e.preventDefault();

	$('.wrapper_search').slideDown(200, function(){
		$('.wrapper_search .searchform').stop().animate({'top': '45%', 'opacity': 1}, 600, 'easeOutExpo');
		setTimeout(function(){
			$('.wrapper_search .searchform').find('input[type=text]').focus();
		}, 500);
	});
});

$('.close_search').on('click', function(e){
	e.preventDefault();

	$('.wrapper_search .searchform').stop().animate({'top': '40%', 'opacity': 0}, 300, 'easeOutExpo');
	$('.wrapper_search').stop().delay(150).slideUp(200);
});

//===== HAMBURGER MENU ==========
$('.hamburger').click(function () {
	$(this).toggleClass('close');
});

//========== HELPERS ==================

// detect devise
function detectDevise() {
	switch (Detect()) {
		case 'desktop' :
		$('body').addClass('desktop');
		break;
		case 'tablet' :
		$('body').addClass('tablet');
		break;
		case 'smartphone' : 
		$('body').addClass('mobile');
		break;
	}
}
// function that calculate the size of the content ant the main 
function setContentAndMainHeight(){
	var siteContent = $('#site_content'),
	main = $('section#main');
	siteContent.css('top', $('header').height());
	main.css('min-height', $(window).height() - $('header').height());

//on resize calculate the new size
$(window).resize(function(event) {
	siteContent.css('top', $('header').height());
	main.css('min-height', $(window).height() - $('header').height());
});
}


//============= RESPONSIVE ==================
//function for responsive debugin dynamique
function responsive_debug(){
	var debug = $('.debug_responsive');
	if(document.URL.match(/ism|devism/)){
		var window_width = $(window).width();
		var breakpoint = foundation_breakpoint(window_width);
		debug.find('span').text(breakpoint+' : '+window_width+'px');

		$(window).on('resize', function(){
			var window_width = $(window).width();
			var breakpoint = foundation_breakpoint(window_width);
			debug.find('span').text(breakpoint+' : '+window_width+'px');
		});

//foundation breakpoint
function foundation_breakpoint(window_width){
	var size;
	if(window_width <= 640){
		size =  'small';
	}else if(window_width >= 641 && window_width <= 1024) {
		size = 'medium';
	}else if(window_width >= 1025 && window_width <= 1440){
		size = 'large';
	}else if(window_width <= 1441 || window_width <= 1920){
		size = 'xlarge';
	}else if(window_width >= 1921) {
		size = 'xxlarge';
	}
	return size;
}
}else{
	debug.css('display', 'none');
}
}

//Lightbox page clinique
// http://lokeshdhakar.com/projects/lightbox2/
lightbox.option({
	'alwaysShowNavOnTouchDevices':true,
	'positionFromTop':200,
	'disableScrolling':true,
	'wrapAround':true
});

//SCROOL TO TOP
scroolToTop("#back_top", 0, 200, "easeOutCubic", 200);
function scroolToTop(e,t,n,r, s){if($(window).scrollTop()>50){$(e).fadeIn(s)}else{$(e).stop().fadeOut(s)}$(window).scroll(function(){if($(this).scrollTop()>50){$(e).fadeIn(s)}else{$(e).stop().fadeOut("fast")}});$(e).click(function(){$("html, body").stop().animate({scrollTop:t},n,r)})}