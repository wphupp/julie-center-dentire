var menuLeft = document.getElementById( 'menu-mobile-s1' ),
menuRight = document.getElementById( 'menu-mobile-s2' ),
menuTop = document.getElementById( 'menu-mobile-s3' ),
menuBottom = document.getElementById( 'menu-mobile-s4' ),
showLeft = document.getElementById( 'showLeft' ),
showRight = document.getElementById( 'showRight' ),
showTop = document.getElementById( 'showTop' ),
showBottom = document.getElementById( 'showBottom' ),
showLeftPush = document.getElementById( 'showLeftPush' ),
showRightPush = document.getElementById( 'showRightPush' ),
body = document.getElementById( 'wrap' );

if(showLeft != null){
	showLeft.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( menuLeft, 'menu-mobile-open' );
		disableOther( 'showLeft' );
	};
}else if(showRight != null){
	showRight.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( menuRight, 'menu-mobile-open' );
		disableOther( 'showRight' );
	};
}else if(showTop != null){
	showTop.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( menuTop, 'menu-mobile-open' );
		disableOther( 'showTop' );
	};
}else if(showBottom != null) {
	showBottom.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( menuBottom, 'menu-mobile-open' );
		disableOther( 'showBottom' );
	};
}else if(showLeftPush != null) {
	showLeftPush.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( body, 'menu-mobile-push-toright' );
		classie.toggle( menuLeft, 'menu-mobile-open' );
		disableOther( 'showLeftPush' );
	};
}else if(showRightPush != null){
	showRightPush.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( body, 'menu-mobile-push-toleft' );
		classie.toggle( menuRight, 'menu-mobile-open' );
		disableOther( 'showRightPush' );
	};
}

function disableOther( button ) {
	if( button !== 'showLeft' && showLeft != null) {
		classie.toggle( showLeft, 'disabled' );
	}
	if( button !== 'showRight' && showRight != null) {
		classie.toggle( showRight, 'disabled' );
	}
	if( button !== 'showTop' && showTop != null) {
		classie.toggle( showTop, 'disabled' );
	}
	if( button !== 'showBottom' && showBottom != null) {
		classie.toggle( showBottom, 'disabled' );
	}
	if( button !== 'showLeftPush' && showLeftPush != null) {
		classie.toggle( showLeftPush, 'disabled' );
	}
	if( button !== 'showRightPush' && showRightPush != null) {
		classie.toggle( showRightPush, 'disabled' );
	}
}