/**
 * InfosignMedia
 * Install Node
 * Install sass
 * sudo npm install -g gulp
 * npm install -g browser-sync
 * npm install
 */

//Change to your local url
 var proxyName       = "base-ism.com";

//activate browser sync
var broswer_sync_activate = false;

 require('es6-promise').polyfill();
 var gulp            = require('gulp'),
 concat              = require('gulp-concat'),
 uglify              = require('gulp-uglify'),
 plumber             = require('gulp-plumber'),
 sourcemaps          = require('gulp-sourcemaps'),
 browserSync         = require('browser-sync'),
 rename              = require('gulp-rename'),
 sass                = require('gulp-sass'),
 autoprefixer        = require('gulp-autoprefixer'),
 htmlmin             = require('gulp-htmlmin'),
 beautifyJs          = require('gulp-jsbeautify'),
 gutil               = require('gulp-util'),
 removeEmptyLines    = require('gulp-remove-empty-lines'),
 markdownpdf         = require('gulp-markdown-pdf'),
 ftp                 = require('gulp-ftp'),
 notify              = require('gulp-notify'),
 sassInheritance     = require('gulp-sass-inheritance'),
 cached              = require('gulp-cached'),
 filter              = require('gulp-filter'),
 reload              = browserSync.reload;
// Compile Our Sass
gulp.task('sass', function() {
  return gulp.src('../styles/sass/**/*.scss')
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(cached('sass'))
  .pipe(sassInheritance({dir: '../styles/sass/'}))
  .pipe(filter(function (file) {
      return !/\/_/.test(file.path) || !/^_/.test(file.relative);
  }))
  .pipe(sass({
    outputStyle: 'compressed'  /*nested compact expanded compressed*/
  }).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['> 0%'],
    cascade: false
  }))
  .pipe(sourcemaps.write("./maps"))
  .pipe(gulp.dest('../styles/css/'))
  .pipe(reload({stream:true}))
  .pipe(notify("SASS compiled"));
});
//to make the javascript more indentent and minify and remove space
gulp.task('js', function() {
  gulp.src(['../js/**/*.js', '!../js/**/*.min.js', '!../js/vendor/*'])
  .pipe(plumber())
  .pipe(cached('js'))
  .pipe(filter(function (file) {
    return !/\.min/.test(file.path);
  }))
  .pipe(uglify())
  .pipe(sourcemaps.init())
  .pipe(rename({suffix:'.min'}))
  .pipe(sourcemaps.write("./maps"))
  .pipe(gulp.dest('../js/'))
  .pipe(reload({stream:true}))
  .pipe(notify("JS compiled"));
});
//browser sync
gulp.task('browser-sync', function() {
  browserSync.init({
    proxy: proxyName,
    port: 8080,
    notify : false,
    files: ["../js/**/*.js", "../styles/css/**/*.css", "../styles/sass/**/*.scss", "../../**/*.php"]
  });
});
// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch(['../js/**/*.js', '!../js/**/*.min.js', '!../js/vendor/*'], ['js']);
  gulp.watch('../styles/sass/**/*.scss', ['sass']);
  // gulp.watch('../../**/*.php');
  // gulp.watch('../../blocks/**/*.scss', ['sass']);
});
// Build task
if(broswer_sync_activate){
  gulp.task('build', ['sass', 'js', 'watch', 'browser-sync']);
}else {
  gulp.task('build', ['sass', 'js', 'watch']);
}
// Default Task
gulp.task('default', ['build']);