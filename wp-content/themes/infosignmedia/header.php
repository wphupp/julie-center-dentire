<?php global $config; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?> prefix="og:http://ogp.me/ns#" fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?> prefix="og:http://ogp.me/ns#" fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?> prefix="og:http://ogp.me/ns#" fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> prefix="og:http://ogp.me/ns#" fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#> <!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, user-scalable=no" />

  <title><?php wp_title(); ?></title>

  <meta property="og:locale" content="fr_CA" />
  <meta name="msapplication-tap-highlight" content="no" />
  <meta name="language" content="<?php echo get_locale(); ?>" />
  <meta name='url' content='<?php echo site_url(); ?>' />

  <!-- COLOR MOBILE HEADER -->
  <!-- Chrome, Firefox OS, Opera and Vivaldi -->
  <meta name="theme-color" content="#000" />
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#000" />
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="#000" />

  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--jQuery modernizr.custom.js-->
<script type="text/javascript" src="<?php echo get_assets().'/js/vendor/modernizr.custom.js'; ?>"></script>
<script type="text/javascript" src="http://www.infosignmedia.com/validator.js"></script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

  <!--nav-mobile-->
  <?php get_template_part( 'content', 'menu-mobile' ); ?>

  <div id="wrap">
    <header>

      <!-- bottom-header -->
      <?php get_template_part( 'content', 'bottom-header' ); ?>

    </header>

    <div id="site_content">
      <section id="main">